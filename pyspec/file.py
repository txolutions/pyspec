# see: https://certif.com/spec_help/scans.html

import os
import re
import sys
import time
import string
import getpass
import logging
import functools
import collections

import numpy

from pyspec.util import StringIO, zip_longest


log = logging.getLogger(__name__)


def _grouper(items, n=8, fillvalue=None):
    """
    Split flat list into list of lists of size n.
    If len(items) is not multiple n, last list is
    filled with fillvalue items
    """
    return zip_longest(*[iter(items)]*n, fillvalue=fillvalue)


def _gen_pieces(path, offset=0):
    with open(path, 'r') as fobj:
        if offset:
            fobj.seek(offset)
        data = fobj.read()
    for idx, piece in enumerate(data.split('\n#')):
        if not idx:  # first block 
            if piece[:1] == '#':  # if it is a header line
                yield piece[1:].strip()
            else: 
                if offset:
                    # maybe block starts with data lines on file update     
                    yield piece.strip()
            continue
        yield piece.strip()


def _load_file(path, headers=None, scans=None, offset=0, last_block=None):
    headers = [] if headers is None else headers
    scans = [] if scans is None else scans
    block = last_block  
    header = headers[-1] if headers else None
    for piece in _gen_pieces(path, offset=offset):
        first_char = piece[:1]
        if 'F' == first_char:
            gid = str(len(headers) + 1)
            block = header = Header(gid)
            headers.append(header)
        elif 'S' == first_char:
            nb, cmd = _build_scan(piece)
            block = scan = Scan(nb, cmd, header)
            scans.append(scan)
            continue

        if block:
            if first_char in string.ascii_letters + '@':
                block._add_piece(piece)
            else:
                block._add_pure_data(piece)

    return headers, scans, block


class SpecFile(object):

    def __init__(self, filename, mode='r'):
        self.name = filename
        self.mode = mode
        # always use path in case CWD changes
        self.path = os.path.abspath(filename)
        if self.readable:
            self.load()
        else:
            self._init()

    def __len__(self):
        return len(self.scans)

    def __iter__(self):
        return iter(self.scans)

    def __getitem__(self, index):
        return self.scans[index]

    def __str__(self):
        s = self._tofile(StringIO())
        s.seek(0)
        return s.read()

    def __repr__(self):
        name = type(self).__name__
        lines = ['{}(name={!r}, path={!r}, mode={!r})'.format(name,
                                                              self.name,
                                                              self.path,
                                                              self.mode)]
        header = None
        for scan in self:
            if header != scan.header:
                header = scan.header
                lines.append('  ' + repr(header))
            lines.append('    ' + repr(scan))
        return '\n'.join(lines)

    def _init(self):
        self._stat = None
        self.headers = []
        self.scans = []
        self.last_block = None
        if self.writable and not self.readable:
            now = time.time()
            header = Header('1')
            header.filename = self.name
            header.epoch = int(now)
            header.date = time.ctime(now)
            header.user = getpass.getuser()
            self.headers.append(header)

    def _tofile(self, fd):
        if self.header and not self:
            self.header._tofile(fd)
            return fd
        header = None
        for scan in self:
            if header != scan.header:
                header = scan.header
                header._tofile(fd)
                fd.write('\n')
            scan._tofile(fd)
            fd.write('\n')
        return fd

    def _update(self, old_size, new_size):
        if old_size > new_size:
            # file was truncated? Reload everything
            return self.load()
        headers, scans, last_block = _load_file(self.path, self.headers, self.scans, offset=old_size, last_block = self.last_block)
        self.last_block = last_block

    @property
    def scan_ids(self):
        return {scan.scan_id:scan for scan in self.scans}

    @property
    def readable(self):
        return 'r' in self.mode or 'a' in self.mode

    @property
    def writable(self):
        return 'w' in self.mode or 'a' in self.mode

    @property
    def header(self):
        """Returns the active (last) header"""
        return self.headers[-1] if self.headers else None

    def update(self):
        stat = self._stat
        curr_stat = os.stat(self.path)
        modified = stat is None or \
                   (stat.st_mtime != curr_stat.st_mtime or \
                    stat.st_size != curr_stat.st_size)
        if not modified:
            return False
        old_size = 0 if stat is None else stat.st_size
        self._update(old_size, curr_stat.st_size)
        self._stat = curr_stat
        return True

    def load(self):
        if not self.readable:
            raise OSError('File not open for reading')
        self._init()
        self.update()

    def save(self):
        if not self.writable:
            raise OSError('File not open for writing')
        with open(self.path, 'w') as fd:
            self._tofile(fd)

    def add_scan(self, nb=None, command=''):
        if nb is None:
            nb = 1 + (self.scans[-1].scan_number if self.scans else 0)
        scan = Scan(nb, command, self.header)
        scan.date = time.ctime()
        self.scans.append(scan)
        return scan


# careful this returns an iterator in python 3
def _parse_fields(line, sep=' '):
    return filter(None, (field.strip() for field in line[2:].split(sep)))


def _parse_file(block, line):
    block.filename = line[2:].strip()


def _parse_epoch(block, line):
    # seconds from 00:00 GMT 1/1/70
    block.epoch = int(line[2:])


def _parse_date(block, line):
    block.date = line[2:].strip()


def _build_scan(line):
    args = line[2:].split(' ', 1)
    return int(args[0]), args[1].strip() if len(args) > 1 else None


def _parse_scan(block, line):
    block.scan_number, block.command = _build_scan(line)


def _parse_comment(block, line):
    block.comments.append(line[2:].strip())


def _parse_unofficial_comment(block, line):
    block.unofficial_comments.append(line.strip())


_G_MAP = {
    0:'G',  # geometry mode, sector, etc.
    1:'U',  # lattice constants and orientation reflections
    3:'UB', # orientation matrix
    4:'Q',  # lambda, frozen angles, cut points, etc.
}

def number(s):
    try:
        return int(s)
    except ValueError:
        return float(s)

def _parse_geometry(block, line):
    pars = line[1:].split(' ', 1)
    try:
        ptype = int(pars[0])
        gtype = _G_MAP.get(ptype, '?')
        try:
            values = [number(v) for v in pars[1].split()]
        except ValueError:
            values = pars[1]
    except ValueError:
        gtype, values = pars

    block.geometry[gtype] = values


def _parse_Q(block, line):
    # reciprocal space position
    block.Q = [number(v) for v in line[2:].split()]


def _parse_time(block, line):
    fields = line[2:].split('  ', 1)
    try:
        acq_time = number(fields[0])
    except ValueError:
        acq_time = fields[0]
    acq_time = {'time': acq_time}
    if len(fields) > 1:
        acq_time['unit'] = fields[1].strip().strip('(').strip(')')
    block.acq_time = acq_time


def _parse_monitor(block, line):
    try:
        block.monitor = number(line[2:])
    except ValueError:
        block.monitor = line[2:]


def _parse_motor_names(block, line):
    block.motor_names.extend(_parse_fields(line, '  '))


def _parse_motor_mnemonics(block, line):
    block.motor_mnemonics.extend(_parse_fields(line))


def _parse_motor_positions(block, line):
    fields = (number(p) for p in _parse_fields(line))
    block.motor_positions.extend(fields)


def _parse_counter_names(block, line):
    block.counter_names.extend(_parse_fields(line, '  '))


def _parse_counter_mnemonics(block, line):
    block.counter_mnemonics.extend(_parse_fields(line))


def _parse_column_nb(block, line):
    # TODO: handle #N n m
    block.nb_columns = int(line[2:])


def _parse_labels(block, line):
    block.labels.extend(_parse_fields(line, '  '))


def _parse_intensity(block, line):
    block.intensity = number(line[2:])


def _parse_results(block, line):
    block.user_results.append(line.strip())


def _parse_user_info(block, line):
    block.user_info.append(line[1:].strip())


def _parse_temperature(block, line):
    block.temperature = line[1:].strip()


def _parse_mca(block, line):
    block.mca_info.append(line[1:].strip())


class Writer:
    """simple helper that writes a line only if not None"""

    def __init__(self, fd):
        self.fd = fd

    def __enter__(self):
        return self

    def __exit__(self, ex_type, ex_value, ex_tb):
        pass

    def writeln(self, prefix, *props, **opts):
        sep = opts.get('sep', '  ')
        props = [str(prop) for prop in props if prop]
        if not props:
            return
        self.fd.write(prefix)
        self.fd.write(sep.join(props))
        self.fd.write('\n')

    def __call__(self, *args, **kwargs):
        return self.writeln(*args, **kwargs)


class prop(property):

    def __init__(self, name, initial=None):
        _name = '_' + name
        if initial is None:
            initial = lambda : None
        def fget(self):
            if not hasattr(self, _name):
                setattr(self, _name, initial())
            return getattr(self, _name)
        def fset(self, new_value):
            setattr(self, _name, new_value)
        fget.__name__ = name
        super(prop, self).__init__(fget, fset)


class Block(object):

    _func_lines = {
        'F': _parse_file,
        'E': _parse_epoch,
        'S': _parse_scan,
        'E': _parse_epoch,
        'D': _parse_date,
        'C': _parse_comment,
        'G': _parse_geometry,
        'Q': _parse_Q,
        'T': _parse_time,
        'M': _parse_monitor,
        'N': _parse_column_nb,
        'O': _parse_motor_names,
        'o': _parse_motor_mnemonics,
        'P': _parse_motor_positions,
        'J': _parse_counter_names,
        'j': _parse_counter_mnemonics,
        'L': _parse_labels,
        'I': _parse_intensity,
        'R': _parse_results,
        'U': _parse_user_info,
        'X': _parse_temperature,
        '@': _parse_mca
    }

    def __init__(self):
        self.comments = []
        self.unofficial_comments = []
        self.user_info = []
        self.user_results = []
        self._end_pos = None

    date = prop('date')

    def _add_piece(self, piece):
        try:
            f = self._func_lines[piece[:1]]
            return f(self, piece)
        except KeyError:
            _parse_unofficial_comment(self, piece)


class Header(Block):

    _re_spec_user = re.compile(r"(?P<title>.*?)\s+User\s+=\s+(?P<user>.*?)$")

    filename = prop('filename')
    epoch = prop('epoch')

    def __init__(self, gid):
        super(Header, self).__init__()
        self.gid = gid
        self.motor_names = []
        self.motor_mnemonics = []
        self.counter_names = []
        self.counter_mnemonics = []

    def _find_user_title(self):
        self._user = None
        self._title = None
        for i, comment in enumerate(self.comments):
            match = self._re_spec_user.search(comment)
            if match:
                self.comments.pop(i)
                self._title = match.group('title')
                self._user = match.group('user')
                break

    @property
    def user(self):
        if not hasattr(self, '_user'):
            self._find_user_title()
        return self._user

    @user.setter
    def user(self, user):
        self.title # force storing the title
        self._user = user

    @property
    def title(self):
        if not hasattr(self, '_title'):
            self._find_user_title()
        return self._title

    @title.setter
    def title(self, title):
        self.user # force storing the user
        self._title = title

    def _tofile(self, fd):
        with Writer(fd) as write:
            write('#F ', self.filename)
            write('#E ', self.epoch)
            write('#D ', self.date)
            if self.user or self.title:
                write('#C ', self.title,
                      ('User = ' + self.user) if self.user else None)
            for c in self.comments:
                write('#C ', c)
            for i, group in enumerate(_grouper(self.motor_names)):
                write('#O{} '.format(i), *group)
            for i, group in enumerate(_grouper(self.motor_mnemonics)):
                write('#o{} '.format(i), *group, sep=' ')
            for i, group in enumerate(_grouper(self.counter_names)):
                write('#J{} '.format(i), *group)
            for i, group in enumerate(_grouper(self.counter_mnemonics)):
                write('#j{} '.format(i), *group, sep=' ')
        return fd

    def __str__(self):
        s = self._tofile(StringIO())
        s.seek(0)
        return s.read()

    def __repr__(self):
        name = type(self).__name__
        fields = []
        if self.filename:
            fields.append('filename={!r}'.format(self.filename))
        if self.date:
            fields.append('date={!r}'.format(self.date))
        if self.user:
            fields.append('user={!r}'.format(self.user))
        if self.title:
            fields.append('title={!r}'.format(self.title))
        return '{}({})'.format(name, ', '.join(fields))


def _build_pure_scalar_data(buff, nb_cols):
    # No MCA: as simple as it can get
    data_0d = numpy.fromstring(buff, sep=' ')
    if not nb_cols:
        nb_cols = len(buff[:buff.find('\n')].split(' '))
    data_0d.shape = -1, nb_cols
    return data_0d


def _build_mixed_data(buff):
    lines = buff.split('\n')

    point_no = 0

    data_0d = []
    data_1d = []
    data_other = []

    mcas = []
    others = []

    for i, line in enumerate(lines):
        line = line.strip()

        if line.startswith('@A'):
            reading_mca = True
            line = line[2:]
            mca_buf = ''

        if reading_mca:
            if line[-1] == '\\':
                mca_buf += ' ' + line[:-1]
            else:
                mca_buf += ' ' + line
                reading_mca = False
                mca_dat = numpy.fromstring(mca_buf, sep=' ')
                mcas.append(mca_dat)
            continue

        d0d = numpy.fromstring(line,sep=' ')
        if d0d.any(): # data line
            data_0d.append(d0d)
            data_1d.append(mcas)
            data_other.append(others)
            mcas = []
            others = []
        else:
            # other, non recognized data are simply appended as strings, not treated
            others.append( line )

    if mcas:
        data_1d.append(mcas)
    if others:
        data_other.append(others)

    return numpy.array(data_0d), data_1d, data_other 

def _build_data(buff, nb_cols):
    if buff.find('@') == -1:
        return _build_pure_scalar_data(buff, nb_cols), [], []
    else:
        return _build_mixed_data(buff)

class Scan(Block):

    def __init__(self, nb, cmd, header=None):
        super(Scan, self).__init__()
        self.scan_number = nb
        self.command = cmd
        self.header = header
        self.geometry = {}
        self.motor_positions = []
        self.labels = []
        self._data = None
        self._datalines = ""

    scan_number = prop('scan_number')
    command = prop('command')
    Q = prop('Q')
    acq_time = prop('acq_time', lambda: {})
    monitor = prop('monitor')
    nb_columns = prop('nb_columns')
    date = prop('date')
    intensity = prop('intensity')
    user_results = prop('user_results', lambda: [])
    user_info = prop('user_info', lambda: [])
    temperature = prop('temperature')
    mca_info = prop('mca_info', lambda: [])

    def _add_piece(self, piece):
        if '\n' in piece:
            piece, datalines = piece.split('\n', 1)
            self._add_pure_data(datalines)

        super(Scan, self)._add_piece(piece)

    def _add_pure_data(self,datablock):
        self._data = None
        self._datalines += datablock + '\n'

    @property
    def nb_rows(self):
        scalar, mca, other = self.data
        if scalar.any():
            return scalar.shape[0]
        else:
            return 0

    @property
    def nb_mcas(self):
        scalar, mcas, other = self.data
        nb_mca = 0
        if mcas:
            for row in mcas:
                nb_mca += len(row) 
        return nb_mca

    @property
    def scan_id(self):
        hid = '' if self.header is None else self.header.gid
        return '{0}.{1}'.format(self.scan_number, hid)

    @property
    def motor_mnemonic_positions(self):
        return dict(zip(self.motor_mnemonics, self.motor_positions))

    @property
    def motor_name_positions(self):
        return dict(zip(self.motor_names, self.motor_positions))

    @property
    def scan_id(self):
        hid = '' if self.header is None else self.header.gid
        return '{0}.{1}'.format(self.scan_number, hid)

    @property
    def motor_mnemonic_positions(self):
        return dict(zip(self.motor_mnemonics, self.motor_positions))

    @property
    def motor_name_positions(self):
        return dict(zip(self.motor_names, self.motor_positions))

    @property
    def has_mca(self):
        for info in self.mca_info:
            if info.startswith('MCA'):
                return True
        return False

    @property
    def data(self):
        if self._data is None:
            if self._datalines == "":
                return numpy.empty(0),[],[]
            self._data = _build_data(self._datalines, self.nb_columns)
        return self._data

    @data.setter
    def data(self, data):
        scalar, mca, other = data
        if scalar is not None:
            scalar = numpy.array(scalar)
        if mca is not None:
            mca = [numpy.array(i) for i in mca]
        self._data = scalar, mca, other
        self.nb_columns = 0 if scalar is None else scalar.shape[-1]

    @property
    def scalar_data(self):
        return self.data[0]

    @property
    def mca_data(self):
        return self.data[1]

    @property
    def other_data(self):
        return self.data[2]

    def __getattr__(self, name):
        if self.header and not name.startswith('_'):
            try:
                return getattr(self.header, name)
            except AttributeError:
                pass
        raise AttributeError('{0!r} object has no attribute {1!r}'
                             .format(type(self).__name__, name))

    def __repr__(self):
        name = type(self).__name__
        cmd = 'cmd={!r}, '.format(self.command) if self.command else ''
        return '{}({}nb={}, id={!r})'.format(name, cmd,
                                             self.scan_number,
                                             self.scan_id)

    def __str__(self):
        s = self._tofile(StringIO())
        s.seek(0)
        return s.read()

    def _tofile(self, fd):
        with Writer(fd) as write:
            write('#S ', self.scan_number, self.command)
            write('#D ', self.date)
            unit = self.acq_time.get('unit')
            if unit:
                unit = '({})'.format(unit)
            write('#T ', self.acq_time.get('time'), unit)
            write('#M ', self.monitor)
            write('#I ', self.intensity)
            if self.geometry:
                for i, g in ((0,'G'), (1, 'U'), (3, 'UB'), (4, 'Q')):
                    geom_line = ' '.join(str(n) for n in self.geometry[g])
                    write('#G{} '.format(i),  geom_line)
            write('#Q ', self.Q)
            if self.motor_positions:
                mot_line = ' '.join(str(p) for p in self.motor_positions)
                write('#P0 ', mot_line)
            for result in self.user_results:
                write('#R ', result)
            for info in self.user_info:
                write('#U ', info)
            write('#X ', self.temperature)
            write('#N ', self.nb_columns)
            write('#L ', *self.labels)
            scalar, mca, other = self.data
            # TODO: write MCA data (and OTHER too)
            for row in scalar:
                fd.write(' '.join(str(i) for i in row))
                fd.write('\n')
            for comment in self.comments:
                write('#C ', comment)
        return fd
