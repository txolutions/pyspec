title: Server API
description: Spec server python API

Spec server is a python server process that supports the same protocol as Spec
server. It's behavior is defined by the chosen plugin and a configuration
file which is loaded at startup.

A running spec server is composed of two parts: the *server core* and
*active plugin*.

The server core exposes a socket connection and translates spec protocol
requests from clients into a plugin calls and plugin replies to spec protocol
replies. It also translates plugin events to spec protocol events.

The plugin is where all the logic happens. A valid spec server plugin has to
obey a simple but clear python API.

Common use cases are:

**Provide a simulation spec server**

for example, the *default* plugin distributed with pyspec provides simulated
motors and variables. You can use a pyspec server during development of your
application if you don't access to a spec session

**Helper to write integration tests for your app**

Write integration tests for your application using a pyspec server.

**Interface with a third party control system**

if you have another control system written in python for example, you can make
it visible to spec by using the plugin as a bridge. On your working spec
session you can just create remote spec motors or counters to the python spec
server as if it was just another spec session.

**Expose a controller**

imagine you have a controller to integrate which would be difficult to write
in spec (for example, a motor controller with a C shared library API or a
controller which is accessible only from Windows). You can write a plugin that
bridges the controller and from your spec session expose your motor as a remote
spec motor.

**Drive a GUI or other application from spec**

you can write a GUI using python and expose a spec server with a plugin specific
to your application. You can configure your spec session to connect to this
*remote spec* and drive the GUI from spec by, for example, writing specific
values to variables.

## Starting spec server

Pyspec server is an optional component of pyspec. If you installed pyspec with
pip, make sure you also install the optional server component with:

```bash
$ pip install pyspec[server]
```

### From the command line

Pyspec comes with an executable called `pyspec-server`. Without arguments, it
expects a configuration file called `spec.toml` to be found on the directory
from which you started the server.

The pyspec source comes with a simple example of a default plugin server in
`examples/server/spec.json`.

So, let's start a pyspec server with:

```bash
$ pyspec-server -c examples/server/spec.json

INFO 2019-05-27 08:57:45,019 root: start time: 2019-05-27 08:57:44.995550
INFO 2019-05-27 08:57:45,019 root: preparing to run...
INFO 2019-05-27 08:57:45,022 pyspec.server.server.tomo: initializing...
INFO 2019-05-27 08:57:45,023 pyspec.server.server.tomo: ...initialized!
INFO 2019-05-27 08:57:45,023 pyspec.server.server: start accepting requests
```

!!! note
    the above command will fail if you a have another spec listening on port
    6510 on the same machine. You can change the pyspec server port by opening
    the spec.json and changing the `"bind": ":6510"` line to another free port.
    Note that the following client code will have to be adapted as well.


So, now from pyspec you should be able to connect to the server it with:

```python
>>> from pyspec import Spec
>>> spec = Spec('localhost:6510') # you could also do: Spec('tomo')
>>> spec.name
'tomo'
>>> spec.get('foo')
'bar'
```

### From code

You can embed a pyspec server programatically in your application by:

```python
import pyspec.server

config = dict()
server = pyspec.server.Server(config)
server.serve_forever()
```

**config** should be a dictionary compatible with the configuration file
explained in the next chapter.

## Configuration file

Pyspec server supports configuration files written in YAML, TOML, JSON or Python.

Using YAML requires `PyYAML` python package to be installed.

Using TOML requires the `toml` python package to be installed.

The important thing is when python reads the file it can extract a (or a list
of) spec configuration(s) as python dictionaries.

Here is an example of a YAML spec server configuration file:

```yaml
# spec.yaml

tomo:                                             # (1) session name
  version: 6.06.10                                # (2) version
  protocol: 4                                     # (3) protocol version
  bind: ":6510"                                   # (4) bind address
  plugin: pyspec.server.plugins.default:Default   # (5) plugin to activate

  variables:                                      # (6) plugin specific config
    foo: bar
```

1. spec session name (optional, defaults to *spec*)
2. version number (optional, defaults to *6.06.10*)
3. protocol version (optional, defaults to 4)
4. bind address (optional, defaults to *:6510*)
5. plugin to activate (optional, defaults to
   *pyspec.server.plugins.default:Default*)
6. plugin specific configuration

Just as a reference, the same example above in TOML:

```ini
# spec.toml

[tomo]
version = "6.06.10"
protocol = 4
bind = ":6510"
plugin = "pyspec.server.plugins.default:Default"

[tomo.variables]
foo = "bar"
```

... in JSON:

```json
{
  "tomo": {
    "version": "6.06.10",
    "protocol": 4,
    "bind": ":6510",
    "plugin": "pyspec.server.plugins.default:Default",
    "variables": {
      "foo": "bar"
    }
  }
}
```

... or as a python file:

```python
# spec.py

name = "tomo"
version = "6.06.10"
bind = ":6510"
plugin = "pyspec.server.plugins.default:Default"

variables = dict(foo="bar")
```

### Multiple spec sessions

The pyspec server configuration file supports multiple spec sessions in a single
server (this is not true for the Python configuration file format).

The only restriction is that they have different names and different bind
addresses.

Here is an example of a YAML configuration file with a `fourc` and a `sixc`
spec sessions:

```yaml
# spec.yaml

fourc:
  bind: ":6510"
  variables:        # plugin specific config
    foo: bar

sixc:
  bind: ":6511"
  variables:        # plugin specific config
    bar: foo

```


## Writting your own plugin

To write a plugin you need to write a python class that either inherits from
`pyspec.server.plugin.RawPlugin` or `pyspec.server.plugin.Plugin`.

!!! note
    Pyspec server uses a [gevent](gevent.org) based concurrency model. This means
    that your plugin must cooperate and be *gevent friendly*.


### Using RawPlugin

`RawPlugin` has a very simple but non pythonic API (you need to implement 3
methods):

* `get(property)` method is called when the client asks for a value of any get_motor_property (ex: *var/SPECD*, *status/ready* or *motor/th/position*).
  *property* follows the [spec protocol](https://certif.com/spec_help/server.html)
  naming. If the plugin recognizes the property, it should return a tuple with
  `(<value>, False)`. False means no error occurred. If the property is not
  recognized it should return `(<error description>, True)`

* `set(property, value)` method is called when the client asks to set a property
  *property* follows the [spec protocol](https://certif.com/spec_help/server.html)
  naming. *Value* is a python object. If the plugin recognizes the property, it
  should return `(None, False)`, otherwise a `(<error description>, True)`

* `run(command)` method is called when a client asks to execute a command, a
  command with return, a function or a function with return. *Command* is a
  string representing the command the client asked to be executed. If the
  plugin recognizes the command it should return `(<result>, False)`, otherwise
  a `(<error description>, True)`

The plugin gives you access to:

* *spec* property: the spec server object
* *config* property: the spec configuration dictionary
* *log* property: a `logging.Log` object you can use to produce log messages

Regarding the configuration, spec server reserves the keys:

* *name*: spec session name
* *bind*: bind address
* *protocol*: protocol version
* *version*: spec version
* *plugin*: a string representation of the plugin callable

These keys will always be present in the config property of your plugin.

You are free to define in your plugin any other extra keys you may need.


Here is simple example which supports getting / setting variables from an
internal dictionary:

```
# mypackage/spec/demo.py

from pyspec.server.plugin import RawPlugin


class Demo(RawPlugin):

    environment = dict(foo="bar")

    def get(self, name):
        """returns a tuple <value: obj>, <error: bool>"""
        if name.startswith('var/'):
            return self.environment[name[4:]], False
	return 'get property {} not supported'.format(name), True

    def set(self, name, value):
        if name.startswith('var/'):
            self.environment[name[4:]] = value
            return None, False
        return 'set property {} not supported'.format(name), True

    def run(self, command):
        return 'unknown command', True
```
If you have a python project called *mypackage* and you store this file in,
for example `mypackage/spec/demo.py` and create a configuration file like this:

```yaml
# spec.yaml
demo:
  plugin: mypackage.spec.demo:Demo
  bind: ":6511"
```

You should be able to launch your server with:
```bash
$ pyspec-server -c spec.yaml
```

and connect a client:
```python
>>> import pyspec
>>> spec = pyspec.Spec('demo')
>>> spec.get('foo')
'bar'
```

### Using Plugin

If you prefer, `Plugin` has a more pythonic API which discriminates setters
and getters for distinct property types.

As with the `RawPlugin`, it is expected that:

* all getters return a tuple `(<value>, False)` on success or
  `(<error description>, True)` in case of error
* all setters return a tuple `(None, False)` on success or
  `(<error description>, True)` in case of error

You can use the following template as a base for your implementation:

```
# mypackage/spec/demo.py

from pyspec.server.plugin import Plugin

class Demo(Plugin):
    def get_status(self, channel):
        raise NotImplementedError

    # variable methods

    def get_variable(self, name):
        raise NotImplementedError

    def set_variable(self, name, value):
        raise NotImplementedError

    # motor methods

    def get_motor_property(self, motor_name, name):
        raise NotImplementedError

    def set_motor_property(self, motor_name, name, value):
        raise NotImplementedError

    def start_one(self, motor_name, value):
        raise NotImplementedError

    def prestart_all(self):
        raise NotImplementedError

    def start_all(self):
        raise NotImplementedError

    def abort_all(self):
        raise NotImplementedError

    def search(self, motor_name, value):
        raise NotImplementedError

    def sync_check(self, motor_name, value):
        raise NotImplementedError

    # counter methods

    def get_scaler_property(self, scaler_name, name):
        raise NotImplementedError

    def start_count(self):
        raise NotImplementedError

    def stop_count(self):
        raise NotImplementedError


    def run(self, command):
        raise NotImplementedError

```

It is not mandatory to implement all methods. If, for example, your plugin does
not support the *run* feature, just skip its implementation.

The `Default` plugin class in `pyspec/server/plugins/default/default.py` is a
good example of a plugin based on the `pyspec.server.plugin.Plugin` class.
