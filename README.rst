SPEC CSS Python
===============

|Pipeline status|
|Code coverage|

The official `SPEC`_ python library.

Documentation can be found `here`_

Features
--------

Without further ado:

.. code-block:: python

    import pyspec

    # create a connection to spec
    fourc = pyspec.Spec('fourc')

    # get spec version
    fourc.version

    # is spec busy?
    fourc.ready

    # work with variables
    scan_number = fourc.get('SCAN_N')
    fourc.set('foo', 'bar')

    # working with motors
    th = fourc.get_motor('th')

    # getting the current motor user position
    th_angle = th.position

    # working with counters
    sec = fourc.get_counter('sec')

    # getting last counter value
    last_acq_time = sec.value

    # work with spec files
    with pyspec.open('scans.dat') as scans:
    s1 = scans['S1']


Credits
-------



.. _SPEC: https://certif.com/content/spec/
.. _here: https://txolutions.gitlab.io/pyspec
.. |Pipeline status| image:: https://gitlab.com/txolutions/pyspec/badges/master/pipeline.svg
    :target: https://gitlab.com/txolutions/pyspec/pipelines
.. |Code coverage| image:: https://gitlab.com/txolutions/pyspec/badges/master/coverage.svg?job=pages
    :target: https://txolutions.gitlab.io/pyspec/coverage
