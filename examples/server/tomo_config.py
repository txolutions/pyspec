name = "tomo"
protocol = 4
version = "6.06.09"
bind = ":6510"
tick = True

def A(dtype):
    import numpy
    return numpy.linspace(99.3, 102, 56789).astype(dtype)


variables = dict(
    TICK=0,
    foo='bar',
    tomo=dict(
        active_motor='chi',
        detector='pilatus1',
        counter='P201_1'
    ),
    arr_char=A('int8'),
    arr_uchar=A('uint8'),
    arr_int16=A('int16'),
    arr_uint16=A('uint16'),
    arr_int32=A('int32'),
    arr_uint32=A('uint32'),
    arr_int64=A('int64'),
    arr_uint64=A('uint64'),
    arr_float32=A('float32'),
    arr_float64=A('float64'),
)

del A

motors = [
    dict(name='th', hard_low_limit=-180, hard_high_limit=180),
    dict(name='tth', hard_low_limit=-180, hard_high_limit=180),
    dict(name='chi', hard_low_limit=-180, hard_high_limit=180),
    dict(name='phi', hard_low_limit=-180, hard_high_limit=180),
]

counters = [
    dict(name="sec", type="SOFTWARE_TIMER")
]
