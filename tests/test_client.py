# -*- coding: utf-8 -*-

import time
import struct
import datetime
import contextlib

import pyspec.client
from pyspec import Spec
from pyspec import protocol

import numpy
import pytest

import conftest

IOS = 'io', ['gevent', 'thread']


def test_client():
    assert Spec is pyspec.client.Spec


@pytest.mark.parametrize(*IOS)
def test_client_creation(spec_server, io):
    cfg = spec_server.config
    port = spec_server.port
    name = cfg.name
    no_name = '__weirdspec__'
    assert no_name != name
    assert port not in protocol.PORT_RANGE

    # spec with weird name is success since it is lazy
    Spec(no_name, io=io)

    spec = Spec(spec_server.address, io=io)
    assert not spec.connected
    assert spec._addr == spec_server.address
    assert repr(spec) == 'Spec({0})'.format(spec_server.address)


@pytest.mark.parametrize(*IOS)
def test_client_before_server(io):
    info = conftest.prepare_spec_server_subprocess()

    spec = Spec(info['address'], io=io)
    assert not spec.connected

    with pytest.raises(OSError):
        spec.name
    server = conftest.start_spec_server_subprocess(info)
    try:
        assert spec.name == server.config.name
    finally:
        server.terminate()


@pytest.mark.parametrize(*IOS)
def test_client(spec_server, io):
    cfg = spec_server.config
    address = spec_server.address
    spec = Spec(address, io=io)
    assert not spec.connected
    assert spec.name == cfg.name
    assert spec.connected
    assert spec.full_name == '{0!r} @ {1}'.format(cfg.name,
                                                  address)

def check_array(a1, a2):
    assert a1.dtype == a2.dtype
    assert a1.size == a2.size
    assert a1.shape == a2.shape
    assert a1.nbytes == a2.nbytes
    assert a1 == pytest.approx(a2)


def test_read_variable_scalar(spec):
    config = spec.spec_server.config
    assert spec.get('VERSION') == config.version
    assert spec.get('SPEC') == config.name
    assert spec.get('tomo') == config.variables['tomo']
    assert spec.get('super_tomo') == config.variables['super_tomo']

    with pytest.raises(pyspec.SpecError):
        spec.get('inexisting')


def test_write_variable_scalar(spec):
    value = 'a weird spec name'
    spec.set('SPEC', value)
    assert spec.get('SPEC') == value
    value = {'this': 'tomo', 'variable': 'is', 'very': 'weird'}
    spec.set('tomo', value)
    assert spec.get('tomo') == value

    class Toto:
        pass

    with pytest.raises(ValueError):
        spec.set('tomo', Toto())


DTYPES = ('char', 'uchar', 'int16', 'uint16', 'int32', 'uint32',
          'int64', 'uint64','float32', 'float64', 'str')

@pytest.mark.parametrize('dim', ['1d', '2d'])
@pytest.mark.parametrize('dtype', DTYPES)
def test_read_variable_array(spec, dim, dtype):
    if dtype == 'str' and dim == '2d':
        pytest.skip('unsupported data type')
    config = spec.spec_server.config
    name = 'arr_{0}_{1}'.format(dtype, dim)
    expected = config.variables[name]
    got = spec.get(name)
    if dtype == 'str':
        assert expected == got
    else:
        check_array(expected, got)


@pytest.mark.parametrize('dtype', ['int', 'float'])
def test_read_variable_list(spec, dtype):
    config = spec.spec_server.config
    var_name = 'list_{0}_1d'.format(dtype)
    expected = numpy.array([config.variables[var_name]])
    got = spec.get(var_name)
    check_array(expected, got)


@pytest.mark.parametrize('dim', ['1d', '2d'])
@pytest.mark.parametrize('dtype', DTYPES)
def test_write_variable_array(spec, dim, dtype):
    if dtype == 'str':
        if dim == '2d':
            pytest.skip('unsupported data type')
        data = ['an', 'example', 'of', '', 'string array']
        name = 'arr_{0}_{1}'.format(dtype, dim)
        spec.set(name, data)
        got = spec.get(name)
        assert data == got
    else:
        numpy_dtype = dtype.replace('char', 'int8')
        if dim == '1d':
            data = numpy.linspace(-12.4, 44, 5000).astype(numpy_dtype)
            data.shape = 1, data.shape[0]
        else:
            data = numpy.linspace(99.3, 102, 5000)
            data = data.reshape((-1, 10)).astype(numpy_dtype)

        name = 'arr_{0}_{1}'.format(dtype, dim)
        spec.set(name, data)
        got = spec.get(name)
        check_array(data, got)


@pytest.mark.parametrize('dtype', ['int', 'float'])
def test_write_variable_list(spec, dtype):
    config = spec.spec_server.config
    var_name = 'list_{0}_1d'.format(dtype)
    data = list(reversed(config.variables[var_name]))
    expected = numpy.array([data])
    spec.set(var_name, data)
    got = spec.get(var_name)
    check_array(expected, got)


def test_variable_event(spec):
    events = []

    def cb(sender, event):
        events.append(event)

    spec.register('var/simple_var', cb)

    spec.io.sleep(0.1)

    assert len(events) == 1
    assert events[-1].channel == 'var/simple_var'
    assert events[-1].value == spec.spec_server.config.variables['simple_var']

    spec.set('simple_var', 56.78)

    spec.io.sleep(0.1)

    assert len(events) == 2
    assert events[-1].channel == 'var/simple_var'
    assert events[-1].value == 56.78

    spec.unregister('var/simple_var', cb)

    spec.set('simple_var', 'something slipped by')

    spec.io.sleep(0.1)

    assert len(events) == 2
    assert events[-1].channel == 'var/simple_var'
    assert events[-1].value == 56.78


def test_variable_event_multiple_listeners(spec):
    events1, events2 = [], []

    def cb1(sender, event):
        events1.append(event)

    def cb2(sender, event):
        events2.append(event)

    spec.register('var/simple_var', cb1)
    spec.register('var/simple_var', cb2)

    spec.io.sleep(0.1)

    assert len(events1) == 1
    assert events1[-1].channel == 'var/simple_var'
    assert events1[-1].value == spec.spec_server.config.variables['simple_var']

    assert len(events2) == 1
    assert events2[-1].channel == 'var/simple_var'
    assert events2[-1].value == spec.spec_server.config.variables['simple_var']

    spec.set('simple_var', 56.78)

    spec.io.sleep(0.1)

    assert len(events1) == 2
    assert events1[-1].channel == 'var/simple_var'
    assert events1[-1].value == 56.78

    assert len(events2) == 2
    assert events2[-1].channel == 'var/simple_var'
    assert events2[-1].value == 56.78

    spec.unregister('var/simple_var', cb1)

    spec.set('simple_var', 'something slipped by')

    spec.io.sleep(0.1)

    assert len(events1) == 2
    assert events1[-1].channel == 'var/simple_var'
    assert events1[-1].value == 56.78

    assert len(events2) == 3
    assert events2[-1].channel == 'var/simple_var'
    assert events2[-1].value == 'something slipped by'

    spec.unregister('var/simple_var', cb2)

    spec.set('simple_var', 'second slip')

    spec.io.sleep(0.1)

    assert len(events1) == 2
    assert events1[-1].channel == 'var/simple_var'
    assert events1[-1].value == 56.78

    assert len(events2) == 3
    assert events2[-1].channel == 'var/simple_var'
    assert events2[-1].value == 'something slipped by'


def test_command(spec):
    assert spec.run('1+2') == 3
    assert spec.run('print("hello")') == None


def test_command_async(spec):
    handler = spec.run('1+2', wait=False)
    assert handler.result() == 3

    start = time.time()
    handler = spec.run('import gevent; gevent.sleep(1)', wait=False)
    assert time.time() < (start + 0.9)
    assert handler.result() == None
    assert time.time() >= (start + 1)


def test_event(spec):
    events = []

    def cb(sender, event):
        events.append(event)

    spec.register_status('ready', cb)

    # wait for first event to arrive
    spec.io.sleep(0.1)

    assert len(events) == 1
    assert events[0].channel == 'status/ready'
    assert events[0].value == 1

    assert spec.run('1+2') == 3
    assert len(events) == 3
    assert events[-2].value == 0
    assert events[-1].value == 1

    spec.unregister_status('ready', cb)
    assert spec.run('1+2') == 3
    assert len(events) == 3
    assert events[-2].value == 0
    assert events[-1].value == 1


def test_counter_properties(spec):
    sec = spec.get_counter('sec')
    assert sec.name == 'sec'
    assert repr(sec) == 'Counter(sec)'
    assert sec.value == 0


def test_counter_events(spec):
    sec = spec.get_counter('sec')
    events = []
    def cb(sender, event):
        events.append(event)

    sec.register('value', cb)

    # wait for first event to arrive
    spec.io.sleep(0.1)

    assert len(events) == 1
    assert events[0].channel == 'scaler/sec/value'
    assert events[0].value == 0


def test_count(spec):
    sec = spec.get_counter('sec')
    acq_time = 0.65

    assert spec.is_counting == False
    start_time = time.time()
    spec.count(acq_time)
    end_time = time.time()
    assert spec.is_counting == False
    assert sec.value == pytest.approx(acq_time)
    assert end_time - start_time >= acq_time
    assert end_time - start_time <= acq_time + 0.2


def test_count_async(spec):
    sec = spec.get_counter('sec')
    acq_time = 0.65

    assert spec.is_counting == False
    start_time = time.time()
    fut = spec.count(acq_time, wait=False)
    end_time = time.time()
    assert end_time - start_time < 0.1
    assert spec.is_counting == True
    assert sec.value < acq_time
    fut.result()
    end_time = time.time()
    assert spec.is_counting == False
    assert sec.value == pytest.approx(acq_time)
    assert end_time - start_time >= acq_time
    assert end_time - start_time <= acq_time + 0.2


def test_motor_properties(spec):
    th = spec.get_motor('th')
    assert th.name == 'th'
    assert repr(th) == 'Motor(th)'
    assert th.position == 0
    assert th.offset == 0
    assert th.dial_position == 0
    assert th.slew_rate == 10000
    assert th.backlash == 0
    assert th.low_limit == -180
    assert th.high_limit == 180
    assert th.step_size == 1000
    assert th.acceleration == 0.125

    th.offset = 10
    assert th.offset == 10
    assert th.position == 10
    assert th.dial_position == 0


def test_motor_events(spec):
    th = spec.get_motor('th')
    events = []
    def cb(sender, event):
        events.append(event)

    th.register('position', cb)

    # wait for first event to arrive
    spec.io.sleep(0.1)

    assert len(events) == 1
    assert events[0].channel == 'motor/th/position'
    assert events[0].value == 0


def test_motor_motion(spec):
    th = spec.get_motor('th')
    assert th.position == 0
    assert th.offset == 0
    assert th.dial_position == 0

    th.move(1.5)
    assert th.position == pytest.approx(1.5)

    th.move(0)
    assert th.position == pytest.approx(0)


def test_motor_motion_async(spec):
    th = spec.get_motor('th')
    assert th.position == 0
    assert th.offset == 0
    assert th.dial_position == 0

    fut = th.move(1.45, wait=False)
    fut.result()
    assert th.position == pytest.approx(1.45)

    th.move(0, wait=False).result()
    assert th.position == pytest.approx(0)


def test_multiple_motor_motion(spec):
    th = spec.get_motor('th')
    assert th.position == 0
    chi = spec.get_motor('chi')
    assert chi.position == 0

    spec.move(th, 1.1, chi, 3.2)

    assert th.position == pytest.approx(1.1)
    assert chi.position == pytest.approx(3.2)

    spec.move('th', 0, 'chi', 0)

    assert th.position == pytest.approx(0)
    assert chi.position == pytest.approx(0)


def test_multiple_motor_motion_async(spec):
    th = spec.get_motor('th')
    assert th.position == 0
    chi = spec.get_motor('chi')
    assert chi.position == 0

    fut = spec.move(th, 1.1, chi, 3.2, wait=False)
    fut.result()

    assert th.position == pytest.approx(1.1)
    assert chi.position == pytest.approx(3.2)

    spec.move('th', 0, 'chi', 0, wait=False).result()

    assert th.position == pytest.approx(0)
    assert chi.position == pytest.approx(0)
