title: Examples
description: Simple examples on how to use PySpec

## Qt

You can easily build a Qt based GUI with pyspec. The only thing to be aware
is that pyspec runs its own version of an event loop - either *thread* based
or *gevent* based (see
[pyspec concurrency models](/api/client-spec#concurrency-models) on how to
create a spec object with different concurrency models).

We advise using the default concurrency model (*thread*) since mixing the
gevent and Qt event loops together is not trivial.

When you register a callback to listen for spec events, on the threaded
concurrency model, the callback will be executed on a separate thread. Qt
doesn't play well when you affect a graphical component (ex: changing a
label text) from a different thread.

The trick is to convert the event into a Qt signal in the pyspec callback.
The Qt signal will be queued to be executed in the main thread so any
callback connected to the signal can safely execute any GUI related code.

The following example illustrates how to do this:

```python
import sys

import pyspec
from PySide2.QtCore import QObject, Signal, Slot
from PySide2.QtWidgets import (QApplication, QWidget, QVBoxLayout,
                               QLabel, QPushButton)


class QSpec(QObject):

    ready = Signal(object)


app = QApplication(sys.argv)
panel = QWidget()
l = QVBoxLayout(panel)
label = QLabel("Hello World")
button = QPushButton('Give work to spec')
l.addWidget(label)
l.addWidget(button)
panel.show()

spec = pyspec.Spec('tomo')
qspec = QSpec()
cb = lambda sender, event: qspec.ready.emit(event)
spec.register_status('ready', cb)

@Slot(int)
def on_ready(event):
    if event.type == event.DISCONNECTED:
        text = 'Spec disconnected!'
    else:
        text = 'ready!' if event.value else 'busy...'
    label.setText(text)

def sleep():
    spec.run('print "start";sleep(5);print "end"', wait=False)

qspec.ready.connect(on_ready)
button.clicked.connect(sleep)
sys.exit(app.exec_())
```

You may also have noticed that the `spec.run()` method is called with the
`wait=False` flag. This prevents the button from being stuck in the pressed
state (and the GUI frozen) until the command is finished.
