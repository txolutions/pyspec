# -*- coding: utf-8 -*-

import os
import time
import socket

import pytest

import pyspec.server

SERVER_TIMEOUT = 5


def wait_for_server_to_start(server):
    start_time = time.time()
    while True:
        if time.time() - start_time > SERVER_TIMEOUT:
            raise RuntimeError('Timeout waiting for server to start')
        try:
            s = socket.create_connection((server.host, server.port))
            s.close()
            break
        except socket.error:
            pass
        time.sleep(0.1)


def prepare_spec_server_subprocess():
    import spec_server_config
    bind = spec_server_config.bind
    host, port = pyspec.server.sanitize_url(bind, host='localhost')
    config_dir = os.path.dirname(os.path.abspath(spec_server_config.__file__))
    config_filename = os.path.join(config_dir, 'spec_server_config.py')
    cmd = ['python', '-m', 'pyspec.server', '-c', config_filename,
           '--log-level', 'INFO']
    return dict(config=spec_server_config, cmd=cmd,
                host=host, port=port,
                address='{0}:{1}'.format(host, port))


def start_spec_server_subprocess(info, wait=True):
    import subprocess
    server = subprocess.Popen(info['cmd'], close_fds=True)
    assert server.returncode is None
    server.host = info['host']
    server.port = info['port']
    server.address = info['address']
    server.config = info['config']
    if wait:
        wait_for_server_to_start(server)
    return server


@pytest.fixture
def spec_server_subprocess():
    info = prepare_spec_server_subprocess()
    server = start_spec_server_subprocess(info)
    yield server
    server.terminate()


@pytest.fixture
def spec_server_thread():
    """Spawns a thread running a SPEC server"""
    import threading
    import spec_server_config
    bind = spec_server_config.bind
    host, port = pyspec.server.sanitize_url(bind, host='localhost')
    config_filename = spec_server_config.__file__[:-1]
    stop = []
    task = threading.Thread(target=pyspec.server.run,
                            args=(config_filename,),
                            kwargs=dict(stop=stop))
    task.host = host
    task.port = port
    task.address = '{0}:{1}'.format(host, port)
    task.config = spec_server_config
    task.start()
    time.sleep(.25)
    yield task
    stop.append(True)
    task.join()


spec_server = spec_server_subprocess



@pytest.fixture(params=['gevent', 'thread'])
def spec(request, spec_server):
    client = pyspec.Spec(spec_server.address, io=request.param)
    client.spec_server = spec_server
    yield client
    client._protocol.disconnect()
    assert not client.connected
