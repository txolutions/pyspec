import sys
import gevent

import logging

from qspecwidget import QSpecWidget

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)

try:
    from PySide2.QtCore import QTimer, QEventLoop
    from PySide2.QtWidgets import QApplication
except ImportError:
    from PySide.QtCore import QTimer, QEventLoop
    from PySide.QtGui import QApplication

if __name__ == '__main__':

    app = QApplication(sys.argv)
    panel = QSpecWidget('fourc',io='gevent')
    panel.show()

    def do_gevent():
        if QEventLoop():
            try:
               gevent.wait(timeout=0.01)
            except AssertionError:
               pass
        else:
            pass

    timer = QTimer()
    timer.timeout.connect(do_gevent)
    timer.start(100)

    sys.exit(app.exec_())
