title: Spec API
description: Spec client python API

This chapter describes how to work with Spec client.
Spec client is the python object that you use to access a remote
spec session in python.

`Spec` class is located in the `pyspec.client` module but since this
is such a useful object, pyspec provides a shortcut in the `pyspec` module.

To connect to a spec server running on your computer (make sure Spec is
running in [server mode](https://certif.com/spec_help/server.html)) simply
type in your python interpreter

```python

>>> import pyspec
>>> spec = pyspec.Spec()

```

Without any arguments, pyspec will try to connect to a spec server session
running on the same machine on port `6510`.

The following alternatives allow you to connect to spec on different
ports, on different machines and even using the session name:

```python
>>> # specific port (ex: 6511) on same machine:
>>> spec = pyspec.Spec(':6510')

>>> # on different machine (always need to give port)
>>> spec = pyspec.Spec('acme.org:6510')

>>> # with session name on same machine
>>> fourc = pyspec.Spec('fourc')

>>> # with session name on different machine
>>> fourc = pyspec.Spec('acme.org:fourc')
```

Spec client object will only fail in case the name is invalid.

!!! important
    Spec client object creation will succeed even if there is no active remote
    Spec server running. The Spec client object will automatically (re)connect
    when the remote Spec server is started. The reconnection mechanism works at
    any moment in the lifetime of the Spec client object.


## Concurrency models

By default, pyspec uses a multi-threaded concurrency model. Pyspec also supports
a [gevent](http://www.gevent.org) based concurrency model. To create a Spec
client object compatible with gevent, simply give an additional keyword argument
`io='gevent'`:

```python
>>> spec = pyspec.Spec(io='gevent')
```

## Properties

### Spec.name

returns the name of the spec session

*Raises*: `SpecError` if there is no connection to Spec

### Spec.version

returns version of spec session

*Raises*: `SpecError` if there is no connection to Spec

### Spec.ready

returns 1 if spec is ready or 0 otherwise

*Raises*: `SpecError` if there is no connection to Spec

### Spec.shell

returns 1 if spec is in a subshell or 0 otherwise

*Raises*: `SpecError` if there is no connection to Spec

### Spec.simulation

returns 1 if spec is in simulation mode or 0 otherwise

*Raises*: `SpecError` if there is no connection to Spec

### Spec.io

return the concurrency model module: *thread* or *gevent*

*Raises*: `SpecError` if there is no connection to Spec

### Spec.full_name

return a spec full name in the format: `'<session name>' @ <host>:<port>`

*Raises*: `SpecError` if there is no connection to Spec

### Spec.connected

return True if connected to spec session or False otherwise

## Spec.is_counting

return True if spec is involved in an acquisition or False otherwise


## Methods

### Spec.get(*variable_name*)

returns the value of the given variable name

*Raises*: `SpecError` if there is no connection to Spec
or the variable does not exist

### Spec.set(*variable_name*, *value*)

sets the value for the given variable

*Raises*: `SpecError` if there is no connection to Spec
or the variable does not exist

### Spec.get_position(*motor_name*)

returns the position for the given motor name

*Raises*: `SpecError` if there is no connection to Spec
or motor does not exist

### Spec.run(*command*, *wait*=True, *timeout*=None)

runs the given command.

If `wait=True` (default) the method blocks
until spec server reports the command has finished. If a timeout is given,
an error is raised if the command surpasses the given timeout. On success
it returns the command result.

If `wait=False` the method returns immediately a `Future` like object which
can be used to retrieve the command result. If a timeout is given, it is
used to construct the Future object which is returned. Therefore the future
will also raise an error if the command surpasses the given timeout.

*Raises*: `SpecError` if wait=True and there is no connection to Spec
or command results in an error or timeout expires before end of command

```python
>>> # run a command, blocking until it finishes: good for 'fast' things
>>> spec.run('check_all_systems')
'All systems ok!'

>>> # run a command asynchronously
>>> fut = spec.run('ascan th 0 90 1000 1', wait=False)

>>> # while scan is running you can do other stuff

>>> # when you want to wait for it to finish:
>>> fut.result()
```

!!! important
    When running asynchronously, if you are using the default the concurrency
    model - *'thread'* - (see [concurrency models](#concurrency-models)), the
    result is a `concurrency.futures.Future` object. When running in *gevent*
    the result is a `gevent.event.AsyncResult`. Be aware they have a similar
    but not identical API


### Spec.abort()

aborts the current macro/function in execution on the spec server. If
there is no macro/function running, nothing happens

*Raises*: `SpecError` if there is no connection to Spec

### Spec.get_motor(*motor_name*)

returns a new [Motor](/api/client-motor/#motor) instance for the given motor name

### Spec.get_counter(*counter_name*)

returns a new [Counter](/api/client-counter/#counter) instance for the given counter name

### Spec.move(*\*pairs_motor_pos*, *wait*=True, *timeout*=None)

moves one or several motors to the given user positions.

the first arguments are pairs motor name (or motor object) and position

If `wait=True` (default) the method blocks until spec server reports the
motion has finished. If a timeout is given, an error is raised if the
command surpasses the given timeout. On success, None is returned.

If `wait=False` the method returns immediately a `Future` like object which
can be used to retrieve the motion result or wait for the motion to finish.
The timeout argument is ignored.

*Raises*: `SpecError` if wait=True and there is no connection to Spec
or motion results in an error or timeout expires before end of motion.

```python
>>> # move synchronosly, blocking until it finishes
>>> spec.move('th', 20, 'chi', 10)

>>> # move asynchronously
>>> fut = spec.move('th', 5, 'chi', 35, wait=False)

>>> # while motors are moving you can do other stuff

>>> # when you want to wait for it to finish:
>>> fut.result()
```

!!! important
    When running asynchronly, if you are using the default the concurrency
    model - *'thread'* - (see [concurrency models](#concurrency-models)), the
    result is a `concurrency.futures.Future` object. When running in *gevent*
    the result is a `gevent.event.AsyncResult`. Be aware they have a similar
    but not identical API

### Spec.count(*value*,  *wait*=True, *timeout*=None)

starts an acquisition. If value is positive, it is interpreted as time in
seconds. If negative, it is interpreted as monitor counts.

If `wait=True` (default) the method blocks until spec server reports the
acquisition has finished. If a timeout is given, an error is raised if the
command surpasses the given timeout. On success, None is returned.

If `wait=False` the method returns immediately a `Future` like object which
can be used to retrieve the acquisition result or wait for the acquisition
to finish. The timeout argument is ignored.

*Raises*: `SpecError` if wait=True and there is no connection to Spec
or acquisituib results in an error or timeout expires before end of motion.

```python
>>> # count for 1.1s synchronosly, blocking until it finishes
>>> spec.count(1.1)

>>> spec.is_counting
False

>>> # count asynchronously
>>> fut = spec.count(0.9, wait=False)

>>> # while spec is acquiring you can do other stuff

>>> spec.is_counting
True

>>> # when you want to wait for it to finish:
>>> fut.result()
```

!!! important
    When running asynchronly, if you are using the default the concurrency
    model - *'thread'* - (see [concurrency models](#concurrency-models)), the
    result is a `concurrency.futures.Future` object. When running in *gevent*
    the result is a `gevent.event.AsyncResult`. Be aware they have a similar
    but not identical API

### Spec.stop_count()

stops the current acquisition. If spec is not acquiring, nothing happens

## Events

Spec object provides signals which you can connect to to be informed of
asynchronous events like variable value changes or status.
See below for the complete list.

!!! important
    A `Event.DISCONNECTED` event is triggered for each callback when spec
    client looses connection with spec.

    Equally, a `Event.VALUE` event is triggered when the connection
    is reestablished

The registration system is weak, meaning that if there is no strong reference
to the callable that has been registered, the registration is discarded.
This means that for the following example the callback function will never be
called because it goes out of scope and will be garbage collected before the
first event even arrives:

```python
import pyspec

def print_spec_ready(spec):
    def cb(sender, event):
        if event.type == event.DISCONNECTED:
            msg = 'disconnected'
        else:
            msg = 'ready' if event.value else 'busy'
        print('{0} is now {1}'.format(sender.name, msg))
    spec.register_status('ready', cb)

tomo = pyspec.Spec('tomo')
print_spec_ready(tomo)

```

There are three registration domains:

1. *status*: global status. Available channels *ready*, *simulate*, *shell*
   and *quit*. Accessible through the `register_status()` and
   `unregister_status()` methods
2. *var*: global variables. Accessible through the `register_variable()` and
   `unregister_variable()` methods
3. *output*: tty and file writing events. Accessible through the
   `register_output()` and `unregister_output()` methods
4. *error*: for error events. Accessible through the `register()` and
   `unregister()` methods

### ready status

You can register a callback to be called every time the remote spec ready
status changes. The callback function receives two arguments:

* *sender*: spec object which triggered the event
* *event*: an Event object containing:
    * *channel*: event channel name (`status/ready`)
    * *type*: event type (Event.VALUE or Event.DISCONNECTED)
    * *value*: event value in case event type is *Event.VALUE* or None otherwise

```python
def on_status_changed(sender, event):
    if event.type == event.DISCONNECTED:
       msg = 'disconnected'
    else:
       msg = 'ready' if event.value else 'busy'
    print('{0} is now {1}'.format(sender.name, msg))

spec.register_status('ready', on_status_changed)
```

Once the callback is registered it is called *immediately* with the current
ready status of the remote spec server

To unregister the callback simply call `spec.unregister_status('status', on_status_changed)`.

### simulation status

You can register a callback to be called every time the remote spec changes
simulation mode. The callback function receives two arguments:

* *sender*: spec object which triggered the event
* *event*: an Event object containing:
    * *channel*: event channel name (`status/simulate`)
    * *type*: event type (Event.VALUE or Event.DISCONNECTED)
    * *value*: 1 if spec entered simulation mode or 0 otherwise.
      None if event type is *Event.DISCONNECTED*

```python
def on_simulate_changed(sender, event):
    sim = 'in' if event.value else 'out of'
    print('{0} is now {1} simulation mode'.format(sender.name, sim))


spec.register_status('simulate', on_simulate_changed)
```

Once the callback is registered it is called *immediately* with the current
simulation mode of the remote spec server

To unregister the callback simply call `spec.unregister_status('simulate', on_simulate_changed)`.

### shell status

You can register a callback to be called every time the remote spec enters
or leaves a subshell. The callback function receives two arguments:

* *sender*: spec object which triggered the event
* *event*: an Event object containing:
    * *channel*: event channel name (`status/shell`)
    * *type*: event type (Event.VALUE or Event.DISCONNECTED)
    * *value*: 1 if spec entered simulation mode or 0 otherwise.
      None if event type is *Event.DISCONNECTED*

```python
def on_shell_changed(sender, event):
    shell = 'in' if event.value else 'out of'
    print('{0} is now {1} subshell'.format(sender.name, shell))


spec.register_status('shell', on_shell_changed)
```

Once the callback is registered it is called *immediately* with the current
shell mode of the remote spec server

To unregister the callback simply call `spec.unregister_status('shell', on_shell_changed)`.

### quit event

You can register a callback to be called when spec quits.
The callback function receives two arguments:

* *sender*: spec object which triggered the event
* *event*: an Event object containing:
    * *channel*: event channel name (`status/quit`)
    * *type*: event type (Event.VALUE or Event.DISCONNECTED)
    * *value*: 1 if spec entered simulation mode or 0 otherwise.
      None if event type is *Event.DISCONNECTED*

```python
def on_quit(sender, event):
    print('{0} quit!'.format(sender.name))


spec.register_status('quit', on_quit)
```

To unregister the callback simply call `spec.unregister_status('quit', on_quit)`.

### error status

You can register a callback to be called every time an error occurs in the remote
spec server. The callback function receives two arguments:

* *sender*: spec object which triggered the event
* *event*: an Event object containing:
    * *channel*: event channel name (`error`)
    * *type*: event type (Event.VALUE or Event.DISCONNECTED)
    * *value*: `No error` string in case of no error or an error description
      otherwise. None if event type is *Event.DISCONNECTED*

```python
def on_error_occured(sender, event):
    if event.type == event.DISCONNECTED:
        print('error in {0}: disconnected'.format(sender.name))
    elif event.value != 'No error':
        print('error in {0}: {1}'.format(sender.name, value))

spec.register('error', on_error_occured)
```

Once the callback is registered it is called *immediately* with the value
*'No error'*.

To unregister the callback simply call `spec.unregister('error', on_error_occured)`.

### variable changes

You can register a callback to be called every time the value of a variable
changes on the remote server.
The callback function receives two arguments:

* *sender*: spec object which triggered the event
* *event*: an Event object containing:
    * *channel*: event channel name (ex: `var/TOMO`)
    * *type*: event type (Event.VALUE or Event.DISCONNECTED)
    * *value*: new variable value. None if event type is *Event.DISCONNECTED*

```python
def on_var_changed(sender, event):
    print('variable {0}.{1!r} changed to {2}'.format(sender.name,
						     event.channel, event.value))

spec.register_variable('TOMO', on_var_changed)
```

Once the callback is registered it is called *immediately* with the current variable
value of the remote spec server

To unregister the callback simply call `spec.unregister_variable('TOMO', on_var_changed)`.

### output changes

You can register a callback to be called every time spec server sends data to
the output (tty) or to a given file.

The callback function receives two arguments:

* *sender*: spec object which triggered the event
* *event*: an Event object containing:
    * *channel*: event channel name (ex: `output/tty`)
    * *type*: event type (Event.VALUE or Event.DISCONNECTED)
    * *value*: event value. None if event type is *Event.DISCONNECTED*

```python
def on_output(sender, event):
    print('spec output: {}'.format(event.value))

spec.register_output('tty', on_output)
```

