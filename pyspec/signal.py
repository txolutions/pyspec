from blinker import Signal as _Signal


class Signal(_Signal):
    """
    receiver: receiver to be notified when a receiver connects or disconnects
              from the signal
    """
    def __init__(self, channel=None, receiver=None):
        super(Signal, self).__init__()
        self.channel = channel
        if receiver:
            self.receiver_connected.connect(receiver.on_connected)
            self.receiver_disconnected.connect(receiver.on_disconnected)


class On(object):

    def __init__(self):
        self.__dict__['_signals'] = {}
        self.__dict__['_groups'] = set()

    def __setattr__(self, name, value):
        if isinstance(value, Signal):
            self._signals[value.channel] = value
        else:
            self._groups.add(value)
        super(On, self).__setattr__(name, value)

    def __getitem__(self, name):
        try:
            return self._signals[name]
        except KeyError:
            for group in self._groups:
                try:
                    return group[name]
                except KeyError:
                    pass
        raise KeyError('Channel {0!r} not in group'.format(name))


class Group(object):
    def __init__(self, cb):
        self._cb = cb
        self._items = {}

    def __getattr__(self, name):
        value = self._cb(name)
        self._items[value.channel] = value
        setattr(self, name, value)
        return value

    def __getitem__(self, name):
        return self._items[name]

