from __future__ import absolute_import

import gevent.lock
import gevent.event
import gevent.select
import gevent.socket

from pyspec.network.common import BaseSocket


class Future(gevent.event.AsyncResult):
    # overwrite on purpose so we can assign members as we please
    pass

Event = gevent.event.Event
sleep = gevent.sleep


class Socket(BaseSocket):

    def __init__(self, host, port, accept=None):
        super(Socket, self).__init__(host, port, accept, gevent.lock.RLock())

    @staticmethod
    def _create_connection(host, port):
        return gevent.socket.create_connection((host, port))

    def _create_read_task(self):
        task = gevent.Greenlet(self._read_loop)
        return task

    @staticmethod
    def _select(*args, **kwargs):
        return gevent.select.select(*args, **kwargs)

    @staticmethod
    def _sleep(t):
        return sleep(t)
