#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

import sys
from setuptools import setup, find_packages


TESTING = any(x in sys.argv for x in ["test", "pytest"])

requirements = ['futures; python_version<"3"',
                'numpy', 'blinker']

setup_requirements = []
if TESTING:
    setup_requirements += ['pytest-runner']
test_requirements = ['pytest', 'pytest-cov']
extras_requirements = {'server': ['gevent']}

setup(
    author="TXOlutions",
    author_email='txo@txolutions.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="Spec CSS python interface",
    entry_points={
        'console_scripts': [
            'pyspec-server = pyspec.server:main',
        ]
    },
    install_requires=requirements,
    license="MIT license",
    long_description="Spec CSS python interface",
    include_package_data=True,
    keywords='spec',
    name='pyspec',
    packages=find_packages(include=['pyspec', 'pyspec.network', 'pyspec.server']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    extras_require=extras_requirements,
    url='https://certif.com',
    version='0.1.0',
    zip_safe=True
)
