
import logging
import pyspec

log = logging.getLogger(__name__)

try:
    from PySide2.QtCore import QObject
    from PySide2.QtWidgets import QWidget, QGridLayout, QLabel, QPushButton
except ImportError:
    from PySide.QtCore import QObject 
    from PySide.QtGui import QWidget, QGridLayout, QLabel, QPushButton

class QSpecWidget(QWidget):

    def __init__(self, spec, io=None, *args):
        QWidget.__init__(self, *args)

        self.specname = spec
        self.spec = pyspec.Spec(spec, io=io)

        self.build_widget()

        self.button.clicked.connect(self.spec_sleep)

        self.cb = lambda sender, event, this=self: QSpecWidget.on_ready(this, event)
        self.var_cb = lambda sender, event, this=self: QSpecWidget.var_changed(this, event)

        self.spec.register_status('ready', self.cb)
        self.spec.register_variable('SCAN_N', self.var_cb)

    def build_widget(self):
        l = QGridLayout()

        self.status_label = QLabel("SPEC status:")
        self.status = QLabel()
        self.scann_label = QLabel("SCAN Number:")
        self.scann = QLabel()

        self.button = QPushButton('Give work to spec')
        l.addWidget(self.status_label, 0, 0)
        l.addWidget(self.status, 0, 1)
        l.addWidget(self.scann_label, 1, 0)
        l.addWidget(self.scann, 1, 1)
        l.addWidget(self.button, 2,0,1,2)
        self.setLayout(l)

    def spec_sleep(self):
        stime = 5
        self.spec.run('printf("sleeping {0} secs...");sleep({0});print "done"'.format(stime), wait=False)

    def var_changed(self, event):
        log.info("variable %s changed now is %s" % (event.channel, event.value))
        self.scann.setText(str(event.value))

    def on_ready(self, event):
        log.info("ready changed")
        if event.type == event.DISCONNECTED:
            status = 'disconnected'
            self.set_status('disconnected')
        else:
            if event.value:
                self.set_status('ready')
            else:
                self.set_status('busy')

    def set_status(self, status):
        if status == 'ready':
            color = '#60f060'
            spec_ready = True
        elif status == 'busy':
            color = '#f0f060'
            spec_ready = False
        else:
            color = '#606060'
            spec_ready = False

        text = self.specname + ' is ' + status
        self.status.setText(text)
        self.status.setStyleSheet('background-color: %s;' % color)
        self.button.setEnabled(spec_ready)

