title: Motor API
description: Spec client Motor python API

Once you have an instance of a spec client object, you can ask for any of its
motors with:

```python
>>> th = spec.get_motor('th')
```

You should not try to instanciate the Motor class directly. The events may
not work as you expect.

You can access any of the motor properties as python object properties:

```python
>>> th.position
34.567
>>> th.acceleration
0.125
>>> # Changing motor properties works too!
>>> th.velocity = 100
```

## Concurrency model

!!! note
    The motor object you get from Spec will inherit its concurrency model object
    (see [spec concurrency models](/api/client-spec/#concurrency-models)).


## Properties

### Motor.name

*read only*

returns the motor name

### Motor.position

*read/write*

returns the current motor position in user units

setting a new value will set the offset so it position matches
the desired value

*Raises*: `SpecError` if there is no connection to Spec

### Motor.dial_position

*read/write*

returns the current motor dial position in dial units

setting a new value will assign a new value to dial position

*Raises*: `SpecError` if there is no connection to Spec

### Motor.offset

*read/write*

returns the motor offset in dial units

setting a new value will assign a new value to offset

*Raises*: `SpecError` if there is no connection to Spec

### Motor.step_size

*read only*

returns the motor steps per unit

*Raises*: `SpecError` if there is no connection to Spec

### Motor.sign

*read only*

returns the sign

*Raises*: `SpecError` if there is no connection to Spec

### Motor.move_done

*read only*

returns True if the motor is busy otherwise False

*Raises*: `SpecError` if there is no connection to Spec

### Motor.low_limit_hit

*read only*

returns True if the motor hit the low limit switch or False otherwise

*Raises*: `SpecError` if there is no connection to Spec

### Motor.high_limit_hit

*read only*

returns True if the motor hit the high limit switch or False otherwise

*Raises*: `SpecError` if there is no connection to Spec

## limits

*read/write*

returns a tuple of two numbers corresponding to the low and high soft
limits

setting a value will change both low and high soft limits

### Motor.fault

*read only*

returns non zero if the motor controllers indicates a hardware motor fault or
zero otherwise

*Raises*: `SpecError` if there is no connection to Spec

### Motor.emergency_stop

*read only*

returns non zero if an emergency stop switch or condition has been activated
or zero otherwise

*Raises*: `SpecError` if there is no connection to Spec

### Motor.unusable

*read only*

returns nonzero if the motor is unusable. The motor may be unusable because
it didn't respond to the presence test, it has been explicitly disabled with
the "disable" option to motor_par(), or it has received an unusable event
from another server.

*Raises*: `SpecError` if there is no connection to Spec

### Motor.base_rate

*read/write*

returns motor base rate in steps/s

setting a new value will change the base rate

*Raises*: `SpecError` if there is no connection to Spec

### Motor.slew_rate

*read/write*

returns motor top velocity in steps/s

setting a new value will change the top velocity

*Raises*: `SpecError` if there is no connection to Spec

### Motor.acceleration

*read/write*

returns the acceleration time in seconds

setting a new value will change the acceleration time

*Raises*: `SpecError` if there is no connection to Spec

### Motor.backlash

*read/write*

returns the backlash

setting a new value will change the backlash

*Raises*: `SpecError` if there is no connection to Spec

## Methods

### Motor.sync(*hard*=None)

TODO

### Motor.start_search(*how*, *home_pos*=None)

TODO

### Motor.move(*position*, *wait*=True, *timeout*=None)

moves the motor to the given user position

If `wait=True` (default) the method blocks
until spec server reports the motion has finished. If a timeout is given,
an error is raised if the command surpasses the given timeout. On success
it returns the command result.

If `wait=False` the method returns immediately a `Future` like object which
can be used to retrieve the motion result. The timeout argument is ignored.

*Raises*: `SpecError` if wait=True and there is no connection to Spec
or motion results in an error or timeout expires before end of motion.

```python
>>> # move synchronosly, blocking until it finishes
>>> th.move(20)
>>> th.position
19.9993

>>> # move asynchronously
>>> fut = th.move(35, wait=False)

>>> # while motor is moving you can do other stuff

>>> # when you want to wait for it to finish:
>>> fut.result()
```

!!! important
    When running asynchronly, if you are using the default the concurrency
    model - *'thread'* - (see [concurrency models](#concurrency-models)), the
    result is a `concurrency.futures.Future` object. When running in *gevent*
    the result is a `gevent.event.AsyncResult`. Be aware they have a similar
    but not identical API

## Events

Spec motor object provides signals which you can connect to to be informed of
asynchronous events like motor position changes or movement started/finished.

The interface is similar to spec:

```python

def cb(sender, event):
    print('position at {0}'.format(event.value))

th.register('position', cb)
```

To unregister the callback simply call `th.unregister('position', cb)`.

It should be possible to subscribe to events on all motor properties.


