from __future__ import absolute_import

import io
import sys
import types
import itertools

# Useful for very coarse version differentiation.
PY2 = sys.version_info[0] == 2
PY3 = sys.version_info[0] == 3
PY34 = sys.version_info[0:2] >= (3, 4)

if PY3:
    from collections import abc
    string_types = str,
    text_type = str
    binary_type = bytes
    integer_types = int,

    def reraise(tp, value, tb=None):
        try:
            if value is None:
                value = tp()
            if value.__traceback__ is not tb:
                raise value.with_traceback(tb)
            raise value
        finally:
            value = None
            tb = None

    StringIO = io.StringIO
    ConnectionError = ConnectionError
    zip_longest = itertools.zip_longest
else:
    import collections as abc
    string_types = basestring,
    text_type = unicode
    binary_type = str
    integer_types = (int, long)

    def exec_(_code_, _globs_=None, _locs_=None):
        """Execute code in a namespace."""
        if _globs_ is None:
            frame = sys._getframe(1)
            _globs_ = frame.f_globals
            if _locs_ is None:
                _locs_ = frame.f_locals
            del frame
        elif _locs_ is None:
            _locs_ = _globs_
        exec("""exec _code_ in _globs_, _locs_""")

    exec_("""def reraise(tp, value, tb=None):
    try:
        raise tp, value, tb
    finally:
        tb = None
""")

    from StringIO import StringIO

    ConnectionError = OSError
    zip_longest = itertools.izip_longest


_str_or_bytes_types = tuple(list(string_types) + [bytes])

def is_str_or_bytes(s):
    return isinstance(s, _str_or_bytes_types)


def is_map(m):
    return isinstance(m, abc.Mapping)


def is_sequence(s):
    return not is_str_or_bytes(s) and isinstance(s, abc.Sequence)


def ensure_binary(s, encoding='utf-8', errors='strict'):
    """Coerce **s** to binary_type.

    For Python 2:
      - `unicode` -> encoded to `str`
      - `str` -> `str`

    For Python 3:
      - `str` -> encoded to `bytes`
      - `bytes` -> `bytes`
    """
    if isinstance(s, text_type):
        return s.encode(encoding, errors)
    elif isinstance(s, binary_type):
        return s
    else:
        raise TypeError("not expecting type '%s'" % type(s))


def ensure_str(s, encoding='utf-8', errors='strict'):
    """Coerce *s* to `str`.

    For Python 2:
      - `unicode` -> encoded to `str`
      - `str` -> `str`

    For Python 3:
      - `str` -> `str`
      - `bytes` -> decoded to `str`
    """
    if not isinstance(s, (text_type, binary_type)):
        raise TypeError("not expecting type '%s'" % type(s))
    if PY2 and isinstance(s, text_type):
        s = s.encode(encoding, errors)
    elif PY3 and isinstance(s, binary_type):
        s = s.decode(encoding, errors)
    return s


def import_module(name):
    """Import module, returning the module after the last dot."""
    __import__(name)
    return sys.modules[name]
