from __future__ import absolute_import

import logging
import weakref
import collections

from pyspec import util
from pyspec import signal
from pyspec import protocol


log = logging.getLogger(__name__)


class Channel(object):

    def __init__(self, name, decode=None):
        self.name = name
        self.decode = (lambda x: x) if decode is None else decode

    def read(self, obj):
        return self.decode(obj._read_channel(self.name))

    def write(self, obj, value):
        return obj._write_channel(self.name, value)

    def __get__(self, obj, type=None):
        if obj is None:
            return self
        return self.read(obj)

    def __set__(self, obj, value):
        self.write(obj, value)


def StatusChannel(name, decode=bool):
    return Channel('status/{0}'.format(name), decode=decode)


def VariableChannel(name, decode=None):
    return Channel('var/{0}'.format(name), decode=decode)


class Event(object):
    VALUE = 0
    DISCONNECTED = 1

    def __init__(self, channel, type=VALUE, value=None):
        self.channel = channel
        self.type = type
        self.value = value


class SignalHandler(object):

    def __init__(self, spec):
        self.spec = spec
        # dict<weakref to receiver: set<signal>>
        self.receivers = collections.defaultdict(set)
        self.protocol.on.event_received.connect(self._handle_event)

    @property
    def protocol(self):
        return self.spec._protocol

    def is_registered(self, receiver_ref, signal):
        if receiver_ref in self.receivers:
            return signal in self.receivers[receiver_ref]
        else:
            False

    def unregister_receiver(self, receiver_ref, signal):
        recvs = signal.receivers
        nb_active_receivers = len(recvs) - (1 if receiver_ref in recvs else 0)
        if nb_active_receivers < 1:
            self.protocol.unregister_channel(signal.channel)

    def register_receiver(self, receiver_ref, signal):
        signals = self.receivers[receiver_ref]
        if signal not in signals:
            signals.add(signal)
            if len(signal.receivers) == 1:
                self.protocol.register_channel(signal.channel)
            return True
        else:
            return False

    def on_disappeared(self, receiver_ref):
        signals = self.receivers.pop(receiver_ref, ())
        for signal in signals:
            self.unregister_receiver(receiver_ref, signal)

    def on_connected(self, signal, receiver, sender, weak):
        receiver_ref = weakref.ref(receiver, self.on_disappeared)
        self.register_receiver(receiver_ref, signal)

    def on_disconnected(self, signal, receiver, sender):
        receiver_ref = weakref.ref(receiver)
        if receiver_ref in self.receivers:
            signals = self.receivers.get(receiver_ref, {})
            if signal in signals:
                signals.remove(signal)
                if not signals:
                    self.receivers.pop(receiver_ref)
                    self.unregister_receiver(receiver_ref, signal)

    def get_signal(self, channel):
        sig = self._find_signal(channel)
        return sig or signal.Signal(channel, self)

    def register(self, channel, cb):
        sig = self.get_signal(channel)
        event = getattr(sig, 'last_event', None)
        sig.connect(cb)
        receiver_ref = weakref.ref(cb)
        if event is not None and not self.is_registered(receiver_ref, sig):
            cb(self.spec, event=event)

    def unregister(self, channel, cb):
        sig = self._find_signal(channel)
        if sig:
            sig.disconnect(cb)

    def broadcast_connection(self):
        signals = {signal for signals in self.receivers.values()
                   for signal in signals}
        for signal in signals:
            self.protocol.register_channel(signal.channel)

    def broadcast_disconnection(self):
        for receiver_ref, signals in self.receivers.items():
            receiver = receiver_ref()
            if receiver is not None:
                for signal in signals:
                    event = Event(signal.channel, type=Event.DISCONNECTED)
                    receiver(self.spec, event)

    def _find_signal(self, channel):
        for signals in self.receivers.values():
            for signal in signals:
                if signal.channel == channel:
                    return signal
        return None

    def _handle_event(self, sender, message):
        channel = message.header.name
        signal = self._find_signal(channel)
        if signal:
            event = Event(channel, value=message.payload)
            signal.last_event = event
            signal.send(self.spec, event=event)


class Spec(object):

    error = Channel('error')
    ready = StatusChannel('ready')
    shell = StatusChannel('shell')
    simulation = StatusChannel('simulate')

    def __init__(self, name=protocol.DEFAULT_ADDR, io=None):
        self._addr = name
        self._log = log.getChild(type(self).__name__)
        self._protocol = protocol.Protocol(name=name, io=io)
        self._protocol.on.connected.connect(self._on_connect)
        self._protocol.on.disconnected.connect(self._on_disconnect)
        self._signal_handler = SignalHandler(self)

    def register(self, channel, cb):
        # ensure we are connected before registering an event handler to
        # prevent a connection at the same time we are registering an event
        if not self._protocol.connected:
            self._protocol.hello()
        self._signal_handler.register(channel, cb)

    def unregister(self, channel, cb):
        self._signal_handler.unregister(channel, cb)

    def register_variable(self, name, cb):
        self.register('var/' + name, cb)

    def unregister_variable(self, name, cb):
        self.unregister('var/' + name, cb)

    def register_status(self, name, cb):
        self.register('status/' + name, cb)

    def unregister_status(self, name, cb):
        self.unregister('status/' + name, cb)

    def register_output(self, name, cb):
        name = name or 'tty'
        self.register('output/' + name, cb)

    def unregister_output(self, name, cb):
        name = name or 'tty'
        self.unregister('output/' + name, cb)

    def __repr__(self):
        return 'Spec({0})'.format(self._addr)

    def _on_connect(self, transport):
        self._signal_handler.broadcast_connection()

    def _on_disconnect(self, transport):
        self._signal_handler.broadcast_disconnection()

    def _on_changed(self, message):
        self._log.info('%r changed to %r', message.header.name, message.payload)

    def _get_channel_name(self, name):
        return name

    def _read_channel(self, channel):
        return self._protocol.read_channel(channel)

    def _write_channel(self, channel, value=None):
        return self._protocol.write_channel(channel, value)

    @property
    def io(self):
        return self._protocol.io

    @property
    def name(self):
        return self._protocol.name

    @property
    def full_name(self):
        return self._protocol.full_name

    @property
    def version(self):
        return self.get('VERSION')

    @property
    def connected(self):
        return self._protocol.connected

    def get_position(self, motor_name):
        """Get the given motor current user position"""
        channel = 'motor/{}/position'.format(motor_name)
        return self._read_channel(channel)

    def get(self, var_name):
        """Get the value of the given spec variable"""
        channel = 'var/{}'.format(var_name)
        return self._read_channel(channel)

    def set(self, var_name, value):
        """Set the value of the given spec variable"""
        channel = 'var/{}'.format(var_name)
        return self._write_channel(channel, value)

    def run(self, cmd, **options):
        return self._protocol.function(cmd, **options)

    def abort(self):
        self._protocol.abort()

    def get_motor(self, name):
        return Motor(self, name)

    def get_counter(self, name):
        return Counter(self, name)

    def move(self, *args, **kwargs):
        wait = kwargs.get('wait', True)
        timeout = kwargs.get('timeout', None)
        all_done = self.io.Future()
        nb_motors = len(args) / 2
        ready = []
        def cb():
            ready.append(1)
            if len(ready) == nb_motors:
                all_done.set_result(None)
        low_args = []
        handlers = {}
        for motor, position in zip(args[::2], args[1::2]):
            motobj = motor if isinstance(motor, Motor) else self.get_motor(motor)
            sig = self._signal_handler.get_signal('motor/{}/move_done'
                                                  .format(motobj.name))
            low_args.extend((motobj.name, position))
            handler = transition_cb(sig, [0, 1, 0], cb)
            handlers[motobj] = handler
        # need to keep a reference to the handlers, otherwise the move_done
        # signals are disconnected
        all_done._handlers = handlers
        self._protocol.start_move(*low_args)
        if wait:
            all_done.result(timeout=timeout)
        else:
            return all_done

    def count(self, value, wait=True, timeout=None):
        all_done = self.io.Future()
        sig = self._signal_handler.get_signal('scaler/.all./count')
        handler = transition_cb(sig, [0, 1, 0], lambda: all_done.set_result(None))
        self._protocol.start_count(value)
        if wait:
            all_done.result(timeout=timeout)
        else:
            return all_done

    @property
    def is_counting(self):
        return self._read_channel('scaler/.all./count') != 0


class BaseGroup(object):

    def __init__(self, spec, name, channel_type=None):
        self.name = name
        self.spec = spec
        if channel_type is None:
            channel_type = type(self).__name__.lower()
        self._channel = '{0}/{1}/{{0}}'.format(channel_type, self.name)

    def __repr__(self):
        return '{}({})'.format(type(self).__name__, self.name)

    def _get_channel_name(self, name):
        return self._channel.format(name)

    def _read_channel(self, name):
        channel = self._get_channel_name(name)
        return self.spec._read_channel(channel)

    def _write_channel(self, name, value):
        channel = self._get_channel_name(name)
        return self.spec._write_channel(channel, value)

    def register(self, name, cb):
        channel = self._get_channel_name(name)
        self.spec.register(channel, cb)

    def unregister(self, name, cb):
        channel = self._get_channel_name(name)
        self.spec.unregister(channel, cb)


class Counter(BaseGroup):

    value = Channel('value')

    def __init__(self, spec, name):
        super(Counter, self).__init__(spec, name, channel_type='scaler')


class Motor(BaseGroup):

    position = Channel('position')
    dial_position = Channel('dial_position')
    offset = Channel('offset')
    step_size = Channel('step_size')
    sign = Channel('sign')
    move_done = Channel('move_done', decode=bool)
    low_limit_hit = Channel('low_limit_hit', decode=bool)
    high_limit_hit = Channel('high_limit_hit', decode=bool)
    low_limit = Channel('low_limit')
    high_limit = Channel('high_limit')
    fault = Channel('motor_fault')
    emergency_stop = Channel('emergency_stop')
    unusable = Channel('unusable')
    base_rate = Channel('base_rate')
    slew_rate = Channel('slew_rate')
    acceleration = Channel('acceleration')
    backlash = Channel('backlash')

    @property
    def limits(self):
        return self.low_limit, self.high_limit

    @limits.setter
    def limits(self, limits):
        self._write_channel('limits', *limits)

    def sync(self, hard=None):
        """if hard is None and a discrepancy is detected,
           spec waits for keyboard input
        """
        if hard is None:
            self._write_channel('sync_check')
        else:
            self._write_channel('sync_check', 1 if hard else 0)

    def start_search(self, how, home_pos=None):
        assert how in ('home', 'home+', 'home-', 'lim+', 'lim-')
        if home_pos is None:
            self._write_channel('search', how)
        else:
            self._write_channel('search', how, home_pos)

    def move(self, position, wait=True, timeout=None):
        all_done = self.spec._protocol.io.Future()
        cb = lambda: all_done.set_result(None)
        handler = transition_cb(self.on.move_done, [0, 1, 0], cb)
        # need to keep a reference to the handler, otherwise the move_done
        # signal is disconnected
        all_done._handler = handler
        self.spec._protocol.start_move(self.name, position)
        return all_done.result(timeout=timeout) if wait else all_done

    def move(self, position, wait=True, timeout=None):
        return self.spec.move(self, position, wait=wait, timeout=timeout)


def transition_cb(signal, states, callback):
    def handler(sender, event):
        value = event.value
        expected_state = states.pop(0)
        if expected_state != value or not states:
            signal.disconnect(handler)
            callback()
            return
    signal.connect(handler)
    return handler
