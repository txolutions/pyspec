import os
import time
import shutil
import tempfile
import collections

import numpy
import pytest

from pyspec.file import SpecFile

_here = os.path.abspath(os.path.dirname(__file__))


def _data_path(*path):
    return os.path.join(_here, 'data', *path)


FileInfo = collections.namedtuple('FileInfo',
                                  'path filename nb_headers date epoch user title '
                                  'motor_names motor_mnemonics '
                                  'counter_names counter_mnemonics '
                                  'scans')
ScanInfo = collections.namedtuple('ScanInfo',
                                  'number scan_id command date acq_time monitor '
                                  'comments labels '
                                  'motor_names motor_mnemonics motor_positions '
                                  'counter_names counter_mnemonics data user_info '
                                  'intensity')


scan1 = FileInfo(
    'scan1.dat', 'pyspec/tests/scan1.dat', 1, 'Wed Feb 14 14:37:23 2018',
    1518615443, 'coyote', 'eurotherm',
    ['Motor 0', 'eurosp'], ['m0', 'eurosp'],
    ['Seconds', 'Monitor', 'Detector', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
    ['sec', 'mon', 'det', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
    [ScanInfo(1, '1.1', 'timescan 1.234 0', 'Wed Feb 14 14:37:55 2018',
              {'time': 1.234, 'unit': 'Seconds'}, None,
              ['Wed Feb 14 14:50:23 2018.  Scan aborted after 3 points.'],
              ['Time', 'Epoch', 'Seconds', 'pview', 'setpt', 'wsetpt', 'op', 'pview2', 'Monitor', 'Detector'],
              ['Motor 0', 'eurosp'], ['m0', 'eurosp'], [-10.55, 345.923],
              ['Seconds', 'Monitor', 'Detector', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
              ['sec', 'mon', 'det', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
              numpy.array(
                  [[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0],
                   [10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0],
                   [20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0]]),
              ['MI0     Current AutoM      Shutter',
               'MI1       -1.00    ON          -1',
               'MI2 Refill in -1 sec, Fill Mode: -1 / Message: -1'], 0.23965),
     ScanInfo(2, '2.1', 'monitorscan -4.321 0', 'Wed Feb 14 14:37:55 2018',
              {}, 4.321,
              ['Wed Feb 14 14:50:23 2018.  Scan aborted after 3 points.'],
              ['Time', 'Epoch', 'Seconds', 'pview', 'setpt', 'wsetpt', 'op', 'pview2', 'Monitor', 'Detector'],
              ['Motor 0', 'eurosp'], ['m0', 'eurosp'], [-10.55, 345.923],
              ['Seconds', 'Monitor', 'Detector', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
              ['sec', 'mon', 'det', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
              numpy.array(
                  [[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0],
                   [10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0],
                   [20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0]]),
              ['MI0     Current AutoM      Shutter',
               'MI1       -1.00    ON          -1',
               'MI2 Refill in -1 sec, Fill Mode: -1 / Message: -1'], 0.23965)
    ]

)


noheader = FileInfo(
    'noheader.dat', None, 0, None, None, None, None, [], [], [], [],
    [ScanInfo(1, '1.', 'timescan 1.234 0', 'Wed Feb 14 14:37:55 2018',
              {'time': 1.234, 'unit': 'Seconds'}, None,
              ['Wed Feb 14 14:50:23 2018.  Scan aborted after 3 points.'],
              ['Time', 'Epoch', 'Seconds', 'pview', 'setpt', 'wsetpt', 'op', 'pview2', 'Monitor', 'Detector'],
              [], [], [],
              [],
              [],
              numpy.array(
                  [[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0],
                   [10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0],
                   [20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0]]),
              [], None)
    ]
)


weird = FileInfo(
    'weird.dat', 'nothing/to/do/path', 2, 'Wed Feb 14 14:37:23 2018',
    1518615443, 'magoo', 'wacky races',
    ['Motor 0', 'eurosp'], ['m0', 'eurosp'],
    ['Seconds', 'Monitor', 'Detector', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
    ['sec', 'mon', 'det', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
    [ScanInfo(1, '1.1', 'timescan 1.234 0', 'Wed Feb 14 14:37:55 2018',
              {'time': 'weird time', 'unit': 'crazy'}, None,
              ['Wed Feb 14 14:50:23 2018.  Scan aborted after 3 points.'],
              ['Time', 'Epoch', 'Seconds', 'pview', 'setpt', 'wsetpt', 'op', 'pview2', 'Monitor', 'Detector'],
              ['Motor 0', 'eurosp'], ['m0', 'eurosp'], [-10.55, 345.923],
              ['Seconds', 'Monitor', 'Detector', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
              ['sec', 'mon', 'det', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
              numpy.array(
                  [[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0],
                   [10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0],
                   [20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0]]),
              [], None),
     ScanInfo(2, '2.1', None, None,
              {}, None,
              [],
              ['Integrated counts'],
              ['Motor 0', 'eurosp'], ['m0', 'eurosp'], [],
              ['Seconds', 'Monitor', 'Detector', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
              ['sec', 'mon', 'det', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
              numpy.array([[1.70344e+07]]),
              [], None),
     ScanInfo(3, '3.1', 'weirdgeometry 1.234 0', 'Thu Feb 15 18:21:12 2018',
              {'time': 1.234, 'unit': 'Seconds'}, None,
              ['Thu Feb 15 18:21:23 2018.  Scan aborted after 1 points.'],
              ['Time', 'Epoch', 'Seconds', 'pview', 'setpt', 'wsetpt', 'op', 'pview2', 'Monitor', 'Detector'],
              ['Motor 0', 'eurosp'], ['m0', 'eurosp'], [-10.55, 345.923],
              ['Seconds', 'Monitor', 'Detector', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
              ['sec', 'mon', 'det', 'pview', 'setpt', 'wsetpt', 'op', 'pview2'],
              numpy.array(
                  [[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]]),
              [], None),
     ScanInfo(1, '1.2', 'h2_timescan 4.321 0', 'Fri Feb 16 16:37:55 2018',
              {'time': 4.321, 'unit': 'Seconds'}, None,
              ['Fri Feb 16 16:39:49 2018.  Scan aborted after 3 points.'],
              ['Time', 'Epoch', 'Seconds', 'c1', 'c2', 'Monitor', 'Detector'],
              ['Theta', 'Chi'], ['th', 'chi'], [12.167, 85.923],
              ['Seconds', 'Monitor', 'Detector', 'Channel A', 'Channel B'],
              ['sec', 'mon', 'det', 'c1', 'c2'],
              numpy.array(
                  [[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0],
                   [10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0],
                   [20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0]]),
              [], None),
    ]
)


timescan1 = FileInfo(
    'timescan1.dat', 'pyspec/tests/timescan1.dat', 1, 'Wed Feb 14 14:37:23 2018',
    1518615443, 'bugsbunny', 'acme',
    ['Motor 0', 'eurosp'], ['m0', 'eurosp'],
    ['Seconds', 'Monitor', 'Detector', 'pv', 'sp', 'wsp', 'op', 'pv2'],
    ['sec', 'mon', 'det', 'pv', 'sp', 'wsp', 'op', 'pv2'],
    [ScanInfo(1, '1.1', 'timescan 1 0', 'Wed Feb 14 14:37:31 2018',
              {'time': 1, 'unit': 'Seconds'}, None,
              ['Wed Feb 14 14:50:23 2018.  Scan aborted after 10 points.'],
              ['Time', 'Epoch', 'Seconds', 'pv', 'sp', 'wsp', 'op', 'pv2', 'Monitor', 'Detector'],
              ['Motor 0', 'eurosp'], ['m0', 'eurosp'], [0, 1167],
              ['Seconds', 'Monitor', 'Detector', 'pv', 'sp', 'wsp', 'op', 'pv2'],
              ['sec', 'mon', 'det', 'pv', 'sp', 'wsp', 'op', 'pv2'],
              numpy.array(
                  [[0.00232005, 9.312, 1.001, 1165.6738, 1200, 1167, 890, 0, 0, 0],
                   [1.21981, 10.661, 1.113, 1165.8591, 1200, 1167, 899, 0, 0, 0],
                   [2.56464, 11.935, 1.009, 1166.0156, 1200, 1167, 903, 0, 0, 0],
                   [3.83868, 13.176, 1, 1166.1864, 1200, 1167, 904, 0, 0, 0],
                   [5.07929, 14.426, 1.009, 1166.3342, 1200, 1168, 911, 0, 0, 0],
                   [6.32897, 15.675, 1.009, 1166.7206, 1200, 1168, 884, 0, 0, 0],
                   [7.57877, 16.925, 1.009, 1166.8568, 1200, 1168, 897, 0, 0, 0],
                   [8.82861, 18.185, 1.009, 1167.163, 1200, 1168, 888, 0, 0, 0],
                   [10.0894, 19.465, 1.008, 1167.3779, 1200, 1168, 889, 0, 0, 0],
                   [11.3682, 20.694, 1.009, 1167.5699, 1200, 1169, 895, 0, 0, 0]]),
              [], None)
    ]
)


FILES = [scan1, weird, timescan1, noheader]


@pytest.fixture
def tmp_dir():
    dir_name = tempfile.mkdtemp()
    yield dir_name
    shutil.rmtree(dir_name)


@pytest.fixture
def tmp_file_info():
    path = 'tmp.dat'
    file_info = timescan1
    src = _data_path(file_info.path)
    dst = _data_path(path)
    shutil.copyfile(src, dst)
    yield file_info._replace(path=path)
    os.remove(dst)


def assert_meta(spec_file, file_info):
    assert len(spec_file) == len(file_info.scans)
    assert len(spec_file.scans) == len(file_info.scans)
    assert len(spec_file.headers) == file_info.nb_headers

    rep = "SpecFile(name='{}', path='{}', mode='r')".format(spec_file.name,
                                                            spec_file.path)
    if file_info.nb_headers:
        header = spec_file.headers[0]
        assert header.filename == file_info.filename
        assert header.title == file_info.title
        assert header.date == file_info.date
        assert header.epoch == file_info.epoch
        assert header.user == file_info.user
        assert header.motor_names == file_info.motor_names
        assert header.motor_mnemonics == file_info.motor_mnemonics

        with pytest.raises(AttributeError):
            header.path

        with pytest.raises(AttributeError):
            spec_file.paths

    assert repr(spec_file).startswith(rep)

    expected_nb_repr_lines = 1 + (len(spec_file)-1) + len(spec_file.headers)
    assert repr(spec_file).count('\n') == expected_nb_repr_lines


def assert_scandata(spec_file, file_info):
    scan_ids = spec_file.scan_ids
    for expected, (idx, scan) in zip(file_info.scans, enumerate(spec_file)):
        scan_by_number = spec_file[idx]
        assert scan_by_number is scan
        assert spec_file[idx] == scan
        assert scan.command == expected.command
        assert scan.scan_number == expected.number
        assert scan.scan_id == expected.scan_id
        assert scan.scan_id in scan_ids
        assert scan is scan_ids[scan.scan_id]
        assert scan.acq_time == expected.acq_time
        assert scan.comments == expected.comments
        assert scan.user_info == expected.user_info
        assert scan.intensity == expected.intensity
        assert scan.date == expected.date
        assert scan.labels == expected.labels
        assert scan.nb_rows == expected.data.shape[0]
        assert scan.nb_columns == expected.data.shape[1]
        assert not scan.has_mca
        if file_info.nb_headers:
            assert scan.motor_names == expected.motor_names
            assert scan.motor_mnemonics == expected.motor_mnemonics
            assert scan.counter_names == expected.counter_names
            assert scan.counter_mnemonics == expected.counter_mnemonics

            name_positions = dict(zip(expected.motor_names,
                                      expected.motor_positions))

            mnemonic_positions = dict(zip(expected.motor_mnemonics,
                                          expected.motor_positions))

            assert scan.motor_name_positions == name_positions
            assert scan.motor_mnemonic_positions == mnemonic_positions

        result = scan.data
        assert result[0] == pytest.approx(expected.data)
        assert result[0] is scan.scalar_data
        assert result[1] == None
        assert result[2] == None


@pytest.mark.parametrize('file_info', FILES, ids=[s.path for s in FILES])
def test_meta(file_info):
    full_path = _data_path(file_info.path)
    spec_file = SpecFile(full_path)
    assert_meta(spec_file, file_info)

    with pytest.raises(OSError):
        SpecFile('/a/non/existing/path')

    with pytest.raises(OSError):
        spec_file.save()


@pytest.mark.parametrize('file_info', FILES, ids=[s.path for s in FILES])
def test_scandata(file_info):
    full_path = _data_path(file_info.path)
    spec_file = SpecFile(full_path)
    assert_scandata(spec_file, file_info)


def test_mca():
    full_path = _data_path('mca.dat')
    spec_file = SpecFile(full_path)

    mca_info = ['MCA %5C', 'CHANN 50 0 49 1', 'CTIME 10 9.7 10']
    acq1 = spec_file[0]
    assert acq1.mca_info == mca_info
    assert acq1.has_mca
    assert acq1.nb_rows == 1

    data = acq1.data
    scalar, mca = data
    mca0 = mca[0]
    assert scalar == None
    assert acq1.scalar_data is None
    assert acq1.mca_data is mca
    assert len(mca) == 1
    assert mca0.shape == (1, 50) # 1 point, 50 channels
    assert pytest.approx(mca0[0][15:20]) == [4061, 3713, 3470, 3155, 3033]

    mca_info = ['MCA %5C', 'CHANN 25 0 24 1', 'CTIME 10 9.7 10']
    acq2 = spec_file[1]
    assert acq2.mca_info == mca_info
    assert acq2.has_mca
    assert acq2.nb_rows == 5

    data = acq2.data
    scalar, mca = data
    mca0 = mca[0]
    assert scalar == None
    assert acq2.scalar_data is None
    assert acq2.mca_data is mca
    assert len(mca) == 1
    assert mca0.shape == (5, 25) # 5 points, 25 channels
    assert pytest.approx(mca0[0][:5]) == [0, 0, 0, 0, 0]
    assert pytest.approx(mca0[1][:5]) == [208, 154, 141, 113, 104]
    assert pytest.approx(mca0[2][:5]) == [23, 23, 19, 12, 11]
    assert pytest.approx(mca0[3][:5]) == [23, 23, 19, 12, 11]
    assert pytest.approx(mca0[4][:5]) == [0, 0, 0, 0, 0]

    mca_info = ['MCA %5C', 'CHANN 25 0 24 1', 'CTIME 5 5.3 5.3',
                'ROI  ROI_1  0  8191', 'ROI  ROI_2  100  8191',
                'ROI  Ni  1130  1570']
    user_info = ['1 mca, 8 scalars, 6 points, mca appear first']
    scan1 = spec_file[2]
    assert scan1.mca_info == mca_info
    assert scan1.user_info == user_info
    assert scan1.has_mca
    assert scan1.nb_rows == 6

    data = scan1.data
    scalar, mca, other = data
    mca0 = mca[0]
    assert scalar.shape == (6, 8)
    assert pytest.approx(scalar) == 6*[[0.65168992, 0.27219, 0.2659,
                                        -0.05106, 57263, 1, 0, 0]]
    assert len(mca) == 1
    assert mca0.shape == (6, 25)
    assert pytest.approx(mca0[0][:5]) == [0, 0, 0, 0, 0]
    assert pytest.approx(mca0[1][:5]) == [208, 154, 141, 113, 104]
    assert pytest.approx(mca0[2][:5]) == [23, 23, 19, 12, 11]
    assert pytest.approx(mca0[3][:5]) == [23, 23, 19, 12, 11]
    assert pytest.approx(mca0[4][:5]) == [0, 0, 0, 0, 0]


def test_update(tmp_file_info):
    full_path = _data_path(tmp_file_info.path)
    spec_file = SpecFile(full_path)

    assert len(spec_file.scans) == len(tmp_file_info.scans)
    assert len(spec_file.headers) == tmp_file_info.nb_headers

    assert spec_file.update() == False

    with open(full_path, 'a') as fobj:
        fobj.write('''
#S 2  append_scan 0 5
#D Wed Feb 15 16:36:11 2018
#T 0.1  (Seconds)
#N 3
#L Time  Epoch  Seconds  pv
0.00232005 9.312 1.001
1.21981 10.661 1.113
2.56464 11.935 1.009
#C Wed Feb 15 16:36:23 2018.  Scan aborted after 3 points.
''')

    assert spec_file.update() == True

    assert len(spec_file.scans) == len(tmp_file_info.scans) + 1
    assert len(spec_file.headers) == tmp_file_info.nb_headers


def test_truncate(tmp_file_info):
    full_path = _data_path(tmp_file_info.path)
    spec_file = SpecFile(full_path)

    assert len(spec_file.scans) == len(tmp_file_info.scans)
    assert len(spec_file.headers) == tmp_file_info.nb_headers

    # the new file must be smaller for SpecFile to interpret
    # has being truncated
    new_file_info = noheader
    src =_data_path(new_file_info.path)
    shutil.copyfile(src, full_path)

    assert spec_file.update() == True

    assert_meta(spec_file, new_file_info)
    assert_scandata(spec_file, new_file_info)


def test_save(tmp_dir):
    fname = os.path.join(tmp_dir, 'tmp.dat')
    spec_file = SpecFile(fname, 'w')

    with pytest.raises(OSError):
        spec_file.update()

    with pytest.raises(OSError):
        spec_file.load()

    assert len(spec_file) == 0
    assert len(spec_file.scans) == 0
    assert len(spec_file.headers) == 1

    header = spec_file.headers[0]
    header.title = 'fourc'
    header.motor_names.extend(['M 1', 'M 2'])
    header.motor_mnemonics.extend(['m1', 'm2'])

    header.counter_names.extend(['C 1', 'C 2'])
    header.counter_mnemonics.extend(['c1', 'c2'])

    header.comments.append('a personal header comment')

    scan = spec_file.add_scan(command='user_scan 3.4')
    assert scan.scan_number == 1
    assert scan.command == 'user_scan 3.4'
    assert len(spec_file) == 1
    assert len(spec_file.scans) == 1
    assert scan is spec_file[0]

    now = time.time()
    scan.date = time.ctime(now)
    scan.motor_positions.extend([55.6, 77.8])
    scan.comments.append('a personal scan comment')
    scan.labels = ['Time', 'Epoch', 'C 1']
    scan.data = [[0.01, 3.5, -66.4],
                 [0.02, 3.8, -5.32]], None, None

    spec_file.save()

    with open(spec_file.path, 'r') as fobj:
        raw = fobj.read()

    raw_lines = raw.split('\n')
    assert raw_lines[0] == '#F {}'.format(fname)
    assert raw_lines[3].startswith('#C fourc  User = ')

    assert raw == str(spec_file)

    spec_file_r = SpecFile(fname, 'r')

    assert len(spec_file_r) == 1
    assert len(spec_file_r.scans) == 1
    assert len(spec_file.headers) == 1

    header_r = spec_file_r.headers[0]
    assert header_r.filename == header.filename
    assert header_r.epoch == header.epoch
    assert header_r.date == header.date
    assert header_r.motor_names == header.motor_names
    assert header_r.motor_mnemonics == header.motor_mnemonics
    assert header_r.counter_names == header.counter_names
    assert header_r.counter_mnemonics == header.counter_mnemonics

    scan_r = spec_file_r[0]

    assert scan_r.scan_number == scan.scan_number
    assert scan_r.date == scan.date
    assert scan_r.command == scan.command
    assert scan_r.motor_positions == scan.motor_positions
    assert scan_r.labels == scan.labels
    assert scan_r.nb_columns == scan.nb_columns
    assert scan_r.data[0] == pytest.approx(scan.data[0])
    assert scan_r.data[1] == scan.data[1]
