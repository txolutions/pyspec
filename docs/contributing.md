# Contributing

There are many ways of contributing.

If you found a bug or you have a good idea for improving pyspec, head over to
pyspec's Gitlab page and create a [new issue](https://gitlab.com/tiagocoutinho/pyspec/issues).

Pull requests are even better! Remember that documentation can also be
improved so don't be shy.

The following chapter provides help on how to start developing pyspec.

# Development

PySpec is actively developed on [GitLab](https://gitlab.com/tiagocoutinho/pyspec).

To start contributing, the first thing to do is fetch the source code.

Start by cloning the public repository:

    $ git clone git://gitlab.com/tiagocoutinho/pyspec

and enter the cloned repository directory:

    $ cd pyspec

## Environment setup

Before starting let me just say that I think software development is highly
personal activity. Each developer as its favorite working methology.

What follows is a personal suggestion on how to setup an environment which
facilitates the software development. It is not pyspec specific and therefore
it can be used for other python projects if you want.

The first principle is that it is always a good idea to develop in a separate
python environment. By isolating yourself for the development/testing of this
project you minimize the chance of breaking your system and at the same time
it enables you to have complete control of the software (dependencies, tools)
you are using.

There are a variety of environments you can use. I suggest you have a look at
[pipenv](http://pipenv.org/). Any other environment like
[conda/miniconda](https://conda.io/) should be fine as well.

I strongly suggest you use version 3 of python but pyspec should work on
python >= 2.7 as well.

Once inside your favorite python environment, install pyspec in development
mode like this:

    $ pip install -e .

You only have to do it once. This will tell python to search for pyspec in
the directory where you cloned pyspec. This means that any modification you
do to the source files will be immediately visible the next time you run
your pyspec program or import pyspec from the python shell.

!!! Recommendation
    I highly recommend that you should only use this installation method in a
    a development environment.

pyspec should now be installed. You can check it with:

    $ python -c "import pyspec; print(pyspec.__version__)"
    0.1.0


