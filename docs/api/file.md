title: Spec file API
description: Spec file python API

# Spec file

This chapter describes how to work with
[Spec files](https://certif.com/spec_help/scans.html).

To open an existing spec file simply type:

```python
>>> from pyspec.file import SpecFile
>>> scan_file = SpecFile('my_scans.dat')
```

The code above opens the given spec file in *read only* mode.

The filename can be an absolute path or relative path. If a relative path is
given, it is relative to the process current working directory.

```python
>>> # access path
>>> scan_file.path
'/home/homer/my_scans.dat'

>>> # repr always gives an informative text about a pyspec object
>>> scan_file
SpecFile(name='my_scans.dat', path='/home/homer/my_scans.dat', mode='r')
  Header(filename='my_scans.dat', date='Wed Feb 14 14:37:23 2018', user='coyote', title='fourc')
    Scan(cmd='timescan 1.234 0', nb=1, id='1.1')
    Scan(cmd='monitorscan -4.321 0', nb=2, id='2.1')

>>> # str shows the equivalent of the file stored on disk.
>>> # we won't show it here... it is too long
>>> print(scan_file)
#F my_scans.dat
#E 1518615443
#D Wed Feb 14 14:37:23 2018
#C fourc  User = coyote
...
```

SpecFile list interface allows you to browse through all the scans in a file:

```python
>>> # size gives the number of scans
>>> len(scan_file)
2
>>> # access specific scan with []
>>> scan0 = scan_file[0]
```

Working with scans is easy:

```
>>> # repr gives scan summary
>>> scan0
Scan(cmd='timescan 1.234 0', nb=1, id='1.1')
>>> # access different parameters
>>> scan.command
'timescan 1.234 0'
>>> # scan data: a tuple of two elements: scalar data, MCA data
>>> scan.data
(<numpy array 2d>, None)
>>> scan.scalar_data
<numpy array 2d>
```

## SpecFile

### Properties

#### SpecFile.name

*read only*

returns the name given in the SpecFile constructor

#### SpecFile.path

*read only*

returns the spec file absolute path

#### SpecFile.mode

*read only*

returns the file read/write/append mode

#### SpecFile.scan_ids

*read only*

returns a dict where key is a string representing scan_id and value is a Scan
object.

#### SpecFile.readable

*read only*

return True if mode is 'r' or 'a'

#### SpecFile.writable

*read only*

returns True if mode is 'w' or 'a'

#### SpecFile.header

*read only*

returns the active (last) header or None if file does not have a header

#### SpecFile.headers

*read only*

returns the list of headers

### Methods

#### SpecFile.update()

If the file has not been modified nothing happens.
If the size is bigger the new scans/headers are appended to the object.
If the size is smaller the file is completely reloaded

This is a naive approach. It works if you know scans are being added in
a consistent manner while you are calling *update()* (ex: spec server is adding new scans periodically).

*Raises*: OSError if object is not in read mode or file disappeared from the filesystem

#### SpecFile.load()

Reload the contents of the file

*Raises*: OSError if object is not in read mode or file disappeared from the filesystem

#### SpecFile.save()

Save the file in the file system

*Raises*: OSError if object is not in write mode or you don't have permissions

#### SpecFile.add_scan(*nb*=None, *command*='')

Appends a new scan.

If *nb* is None it is calculated as the last scan number + 1 or 1 if it is the first scan.

Returns a new Scan object. It is attached to the last active header.

*Raises*: OSError if object is not in write mode

## Header

### Properties

#### Header.filename

*read/write*

returns the spec filename as specified in the first *#F* field

#### Header.date

*read/write*

returns the date as specified in the first *#D* field

#### Header.user

*read/write*

returns the user name or None if no user defined

#### Header.title

*read/write*

returns the title (spec session name) or None if no title defined

#### Header.epoch

*read/write*

returns the epoch as specified in the first *#E* field

#### Header.motor_names

*read/write*

returns the list of motor names as specified in the first *#O* field

#### Header.motor_mnemonics

*read/write*

returns the list of motor mnemonics as specified in the first *#o* field

#### Header.counter_names

*read/write*

returns the list of counter names as specified in the first *#J* field

#### Header.counter_mnemonics

*read/write*

returns the list of counter mnemonics as specified in the first *#j* field

## Scan

### Properties

#### Scan.scan_number

*read/write*

returns the scan number

#### Scan.command

*read/write*

returns the command or None if there was no command is present

#### Scan.header

*read/write*

returns the header associated to the scan

#### Scan.Q

*read/write*

returns a list of Q (H K L) parameters or empty list if no Q is present

#### Scan.acq_time

*read/write*

returns a dict with time and unit keys or empty dict if no Time is present

#### Scan.monitor

*read/write*

returns the monitor counts used for this scan or None

#### Scan.nb_columns

*read only*

returns the number of columns in the scan

#### Scan.nb_rows

*read only*

returns the number of rows in the scan

#### Scan.date

*read/write*

returns the scan date

#### Scan.intensity

*read/write*

returns the multiplicative intensity-normalization factor

#### Scan.user_results

*read/write*

returns the list of user results

#### Scan.user_info

*read/write*

returns the list of user defined information

#### Scan.temperature

*read/write*

returns the temperature information or None

#### Scan.geometry

*read/write*

returns dict with keys *G*, *U*, *UB* and *Q* representing the geometry
information or an empty dict if there is no geometry information

#### Scan.motor_positions

*read/write*

returns a list of motor positions aligned with the *Header.motor_names* and *Header.motor_mnemonics*

#### Scan.labels

*read/write*

returns list of labels representing the scan columns

#### Scan.scan_id

*read only*

returns the scan_id string. It is just scan_number + '.' + header.gid

#### Scan.motor_mnemonic_positions

*read only*

returns dict where key is motor mnemonic and value its position

#### Scan.motor_name_positions

*read only*

returns dict where key is motor name and value its position

#### Scan.data

*read/write*

returns scan data. A tuple of two elements: scalar data (or None) and
MCA data (or None)

When present scalar data is a numpy array with shape:
`(nb_scan_rows, nb_scan_columns)`

When present, MCA data is a list of 1D detector(s) data. Each element
is a numpy array with shape `(nb_scan_rows, nb_mca_channels)`

#### Scan.scalar_data

return scan scalar data (or None). It is an alias of `scan.data[0]`

#### Scan.mca_data

return scan MCA data (or None). It is an alias of `scan.data[1]`

