from __future__ import absolute_import

import sys
import socket
import logging

from pyspec import util
from pyspec import signal

__all__ = ['BaseTransport']

RECONNECTION_TIME = 0.25

PORT_FIRST = 6510
PORT_LAST = 6530
PORT_RANGE = tuple(range(PORT_FIRST, PORT_LAST+1))


class BaseTransport(object):

    def __init__(self, host, port):
        name = type(self).__name__
        self.host = host
        self.port = port
        self._log = logging.getLogger('{0}.{1}.{2}'.format(name, host, port))
        self.on = signal.On()
        self.on.connected = signal.Signal()
        self.on.disconnected = signal.Signal()
        self.on.data = signal.Signal()

    def connect(self):
        raise NotImplementedError

    def disconnect(self):
        raise NotImplementedError

    @property
    def connected(self):
        raise NotImplementedError

    def future(self):
        raise NotImplementedError

    def read(self, size):
        raise NotImplementedError

    def write(self, buff):
        raise NotImplementedError

    def fileno(self):
        raise NotImplementedError

    def __repr__(self):
        name = type(self).__name__
        return '{0}({1}:{2})'.format(name, self.host, self.port)


class BaseSocket(BaseTransport):

    def __init__(self, host, port, accept, lock):
        super(BaseSocket, self).__init__(host=host, port=port)
        self._conn = None
        self._connected = False
        self._read_task = None
        self._accept = accept
        self._lock = lock

    def __del__(self):
        self.close()

    @staticmethod
    def _create_connection(host, port):
        raise NotImplementedError

    def _create_read_task(self, f=None):
        raise NotImplementedError

    def _verify_connection(self):
        if self._accept is None:
            return True
        try:
            return self._accept(self)
        except:
            return False
        
    @staticmethod
    def _select(*args, **kwargs):
        raise NotImplementedError

    @staticmethod
    def _sleep(t):
        raise NotImplementedError

    @property
    def connected(self):
        return self._connected

    def connect(self):
        if self._conn:
            return
        self._connect()
        self._connected = True
        self._read_task = self._create_read_task()
        self._read_task.start()

    def _connect(self):
        with self._lock:
            if self._conn is not None:
                return
            host, port = self.host, self.port
            try:
                self._conn = self._create_connection(host, port)
            except socket.gaierror:
                for port in PORT_RANGE:
                    try:
                        self._conn = self._create_connection(host, port)
                        if self._verify_connection():
                            break
                        else:
                            self._conn = None
                    except socket.error:
                        pass
                else:
                    name = '{}:{}'.format(self.host, self.port)
                    raise util.ConnectionError('Cannot connect {0!r}'
                                               .format(name))
            except socket.error:
                name = '{}:{}'.format(self.host, self.port)
                raise util.ConnectionError('Cannot connect to {0!r}'
                                           .format(name))
        self.on.connected.send()

    def disconnect(self):
        #user disconnected explicitly
        if not self._conn:
            return
        self._connected = False
        if self._read_task:
            #TODO: notify to kill task
            #self._read_task.kill()
            self._read_task = None
        self._disconnect()

    def _disconnect(self):
        # disconnected because of error
        with self._lock:
            if self._conn is None:
                return
            self._conn.close()
            self._conn = None
        self.on.disconnected.send()

    def close(self):
        self.disconnect()

    def read(self, size):
        self.connect()
        missing, data = size, []
        while missing > 0:
            buff = self._conn.recv(missing)
            if not buff:
                self._disconnect()
                raise util.ConnectionError('remote end disconnected')
            missing -= len(buff)
            data.append(buff)
        data = b''.join(data)
        self._log.debug('read %d bytes', len(data))
        return data

    def _read_loop(self):
        while True:
            if self._connected:
                # ensure socket is connected
                try:
                    self._connect()
                except:
                    self._sleep(RECONNECTION_TIME)
                    continue
            else:
                break
            try:
                self._select((self,) ,(), ())
            except:
                pass
            else:
                self.on.data.send()

    def write(self, data):
        self.connect()
        self._log.debug('send %d bytes', len(data))
        try:
            return self._conn.sendall(data)
        except BrokenPipeError:
            self.disconnect()
            util.reraise(sys.exc_info())

    def fileno(self):
        self.connect()
        return self._conn.fileno()
