# -*- coding: utf-8 -*-

"""
Python SPEC server

configuration options:

.. code-block:: toml

    [fourc]
    # standard: V4 on 0:6510 with default plugin

    [sixc]
    protocol = 4
    version = "6.07.10"
    bind = ":6511"
    plugin = "pyspec.server.plugins.default.Default"

    # following are interpreted by each plugin
    [sixc.variables]
    hello = "world"

    [[sixc.motors]]
    name = "th"
    hard_low_limit = -180
    hard_high_limit = 180
"""

from __future__ import absolute_import

import datetime
_START_TIME = datetime.datetime.now()
import os
import logging

import gevent.server

from pyspec import util
from pyspec import protocol


log = logging.getLogger(__name__)


def decode_command(command, sep='\x00'):
    """replace weird \x00 with (,) function syntax"""
    if command.find(sep) == -1:
        return command
    return command.replace(sep, '(', 1).replace(sep, ',') + ')'


class Handler(object):

    def __init__(self, spec, sock, addr):
        self.spec = spec
        self.rfile = sock.makefile('rb')
        self.wfile = sock.makefile('wb', 0)
        self.log = log.getChild('{0}.{1}'.format(type(self).__name__, addr))
        self.event_channels = set()

    def read_message(self):
        return protocol.read_message(self.rfile)[0]

    def create_message(self, command, name='', payload=b'',
                       serial_number=None, error=False):
        version = self.spec.config['protocol']
        return protocol.create_message(command, name=name,
                                       version=version, payload=payload,
                                       serial_number=serial_number,
                                       error=error)

    def write_message(self, message):
        data = protocol.pack_message(message)
        self.wfile.write(data)

    def write(self, command, name='', payload=b'',
              serial_number=None, error=False):
        message = self.create_message(command, name=name, payload=payload,
                                      serial_number=serial_number, error=error)
        self.write_message(message)

    def handle_request(self):
        try:
            message = self.read_message()
            self.handle_message(message)
            return True
        except util.ConnectionError:
            return False

    def handle_message(self, message):
        command = message.header.command
        if command == protocol.HELLO:
            return self.handle_hello(message)
        elif command == protocol.CHAN_READ:
            return self.handle_read(message)
        elif command == protocol.CHAN_SEND:
            return self.handle_write(message)
        elif command == protocol.REGISTER:
            return self.handle_register(message)
        elif command == protocol.UNREGISTER:
            return self.handle_unregister(message)
        elif command in (protocol.CMD, protocol.CMD_WITH_RETURN,
                         protocol.FUNC, protocol.FUNC_WITH_RETURN):
            return self.handle_cmd(message)

    def handle_cmd(self, message):
        cmd = message.header.command
        func = decode_command(message.payload)
        value, error = self.spec.run(func)
        if cmd in (protocol.CMD_WITH_RETURN, protocol.FUNC_WITH_RETURN):
            n = message.header.serial_number
            name = message.header.name
            reply = self.write(protocol.REPLY, name=name, error=error,
                               payload=value, serial_number=n)

    def handle_register(self, message):
        channel = message.header.name
        self.log.info('register events for %s', channel)
        self.event_channels.add(channel)
        value, error = self.spec.get(channel)
        self.send(channel, value)

    def handle_unregister(self, message):
        channel = message.header.name
        self.log.info('unregister events for %s', channel)
        if channel in self.event_channels:
            self.event_channels.remove(channel)

    def handle_read(self, message):
        n = message.header.serial_number
        name = message.header.name
        value, error = self.spec.get(util.ensure_str(name))
        reply = self.write(protocol.REPLY, name=name, error=error,
                           payload=value, serial_number=n)

    def handle_write(self, message):
        n = message.header.serial_number
        name = message.header.name
        var_name = util.ensure_str(name)
        value = message.payload
        value, error = self.spec.set(var_name, value)
        # reproduce exactly spec behavior: if set variable results in an error
        # reply, if not don't reply
        if error:
            reply = self.write(protocol.REPLY, name=name, error=error,
                               payload=value, serial_number=n)

    def handle_hello(self, message):
        name = util.ensure_binary(self.spec.name)
        n = message.header.serial_number
        self.write(protocol.HELLO_REPLY, payload=name, serial_number=n)

    def send(self, channel, value):
        if channel in self.event_channels:
            n = 666
            self.log.info('send event channel:%r value=%s', channel, value)
            message = self.write(protocol.EVENT, name=channel,
                                 payload=value, serial_number=n)


def plugin_class(name):
    if util.is_str_or_bytes(name):
        package_name, class_name = name.rsplit(':', 1)
        package = util.import_module(package_name)
        return getattr(package, class_name)
    return name


class Spec(object):

    def __init__(self, config):
        self.config = config
        self.log = log.getChild(self.name)
        self.handlers = set()
        self.initialize()

    def initialize(self):
        self.log.info('initializing...')
        plugin_type = self.config['plugin']
        self.plugin = plugin_class(plugin_type)(self)
        self.log.info('...initialized!')

    @property
    def version(self):
        return self.config['version']

    @property
    def protocol(self):
        return self.config['protocol']

    @property
    def name(self):
        return self.config['name']

    def get(self, name):
        return self.plugin.get(name)

    def set(self, name, value):
        return self.plugin.set(name, value)

    def send(self, channel, value):
        for handler in self.handlers:
            handler.send(channel, value)

    def run(self, cmd):
        return self.plugin.run(cmd)

    def handle_client(self, sock, addr):
        self.log.info('New connection from %s:%s' % addr)
        handler = Handler(self, sock, addr)
        self.handlers.add(handler)
        try:
            while handler.handle_request():
                continue
            self.log.info('Client disconnected: %s:%s' % addr)
        finally:
            self.handlers.remove(handler)


def sanitize_url(url, host='0', port=protocol.DEFAULT_PORT):
    if not url:
        pass
    elif ':' in url:
        host, port = url.split(':', 1)
    else:
        host = url
    if not host:
        host = '0'
    if not port:
        port = DEFAULT_PORT
    port = int(port)
    return host, port


_DEFAULT_CLASS = 'pyspec.server.plugins.default:Default'
_DEFAULT_CONFIG = dict(name='spec', bind=':6510', protocol=4,
                       version='6.06.10', plugin=_DEFAULT_CLASS,
                       config={})


def Server(config):
    cfg = dict(_DEFAULT_CONFIG)
    cfg.update(config)
    bind = sanitize_url(cfg['bind'])
    spec = Spec(cfg)
    srv = gevent.server.StreamServer(bind, handle=spec.handle_client)
    srv.spec = spec
    return srv


def load_config(filename):
    if not os.path.exists(filename):
        raise ValueError('configuration file does not exist')
    ext = os.path.splitext(filename)[-1]
    if ext.endswith('toml'):
        from toml import load
    elif ext.endswith('yml') or ext.endswith('.yaml'):
        import yaml
        def load(fobj):
            return yaml.load(fobj, Loader=yaml.Loader)
    elif ext.endswith('json'):
        from json import load
    elif ext.endswith('py'):
        # python only supports a single server definition
        def load(fobj):
            r = {}
            exec(fobj.read(), None, r)
            return [r]
    else:
        raise NotImplementedError
    with open(filename)as fobj:
        return load(fobj)


def servers(config):
    if isinstance(config, dict):
        config = [dict(item, name=key)
                  for key, item in config.items()]
    return [Server(item) for item in config]


def serve_forever(spec_servers, stop=None):
    tasks = [gevent.spawn(spec_server.serve_forever)
             for spec_server in spec_servers]
    log.info('start accepting requests')
    try:
        while not stop:
            gevent.sleep(1)
    except KeyboardInterrupt:
        log.info('Ctrl-C pressed. Bailing out')
    for spec_server in spec_servers:
        spec_server.stop()
    gevent.wait(tasks, timeout=0.1)


def run(filename, stop=None):
    logging.info('preparing to run...')
    config = load_config(filename)
    spec_servers = servers(config)
    serve_forever(spec_servers, stop)


def main(args=None):
    import argparse
    parser = argparse.ArgumentParser()
    default = '0:{0}'.format(protocol.DEFAULT_PORT)
    parser.add_argument('-c', help='configuration file',
                        dest='config_file',
                        default='./spec.toml')
    parser.add_argument('--log-level', help='log level', type=str,
                        default='INFO',
                        choices=['DEBUG', 'INFO', 'WARN', 'ERROR'])

    options = parser.parse_args(args)

    log_level = getattr(logging, options.log_level.upper())
    log_fmt = '%(levelname)s %(asctime)-15s %(name)s: %(message)s'
    logging.basicConfig(level=log_level, format=log_fmt)
    logging.info('start time: %s', _START_TIME)
    run(options.config_file)


if __name__ == "__main__":
    main()
