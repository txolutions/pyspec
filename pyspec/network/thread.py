from __future__ import absolute_import

import time
import select
import socket
import threading
import concurrent.futures

from pyspec.network.common import BaseSocket

Future = concurrent.futures.Future
Event = threading.Event
sleep = time.sleep


def Task(target):
    task = threading.Thread(target=target)
    task.daemon = True
    return task


class Socket(BaseSocket):

    def __init__(self, host, port, accept=None):
        super(Socket, self).__init__(host, port, accept, threading.Lock())

    @staticmethod
    def _create_connection(host, port):
        return socket.create_connection((host, port))

    def _create_read_task(self):
        return Task(self._read_loop)

    @staticmethod
    def _select(*args, **kwargs):
        return select.select(*args, **kwargs)

    @staticmethod
    def _sleep(t):
        return sleep(t)
