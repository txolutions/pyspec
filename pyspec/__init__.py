# -*- coding: utf-8 -*-

"""Top-level package for SPEC CSS Python."""

from __future__ import absolute_import

from .client import Spec
from .protocol import SpecError


__version__ = '0.1.0'
