import sys
import logging

try:
    from PySide2.QtWidgets import QApplication
except ImportError:
    from PySide.QtGui import QApplication

from qspecwidget import QSpecWidget

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    panel = QSpecWidget('fourc')
    panel.show()
    sys.exit(app.exec_())

