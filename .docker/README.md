# Docker images

This directory contains the different Dockerfiles used by
pyspec's gitlab CI.

## How to build & push

You only need to do this if you want to deploy another image for
a specific CI usage.

From this directory, for a specific python version (example 3.7)
do:

```bash

PY_VER="37"
IMAGE_NAME="registry.gitlab.com/tiagocoutinho/pyspec:py$PY_VER"

docker build -t $IMAGE_NAME python-$PY_VER
docker login registry.gitlab.com
docker push $IMAGE_NAME
```
