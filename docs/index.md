# Welcome to PySpec

A [python](http://python.org) library to
[Spec CSS](https://certif.com/content/spec/).
Without further ado:

![spec in action](./img/spec_console3.svg)

[![pipeline][pipeline_badge]](https://gitlab.com/tiagocoutinho/pyspec/pipelines)
[![coverage][coverage_badge]](https://tiagocoutinho.gitlab.io/pyspec/coverage)
[![version][version_badge]](https://pypi.org/project/pyspec/#files)
[![status][status_badge]](https://pypi.org/project/pyspec/)
![python][python_badge]
[![downloads][downloads_month_badge]](https://pypi.org/project/pyspec/#files)

Check out the [getting started](user-guide) to learn how to
to install PySpec. For the eager to start, there is also a
[quick start](user-guide#quick-start) that can help you with
your first steps in the PySpec world.

Head straigth for the [Spec client](api/client-spec.md) or
[Spec file](api/file.md) chapters to learn the details on how to
connect to a remote spec server or how to work with Spec files.

Found a bug, need a feature or want to contribute? Check
[here](contributing)!


## Beloved features

In the confort of your favorite programming language (python ;-), this library
allows you to:

* Connect to a remote spec session with python

    * access spec variables (read/write)
    * execute macros/functions (sync/async)
    * access motor parameters
    * move motors (sync/async)
    * access counters
    * count (sync/async)
    * in the confort of a multi-threaded or gevent concurrency models
    * monitor spec status, output, variables, motor parameters through
      its event system

* Manipulate Spec files

    * Read and manipulate existing spec files
    * Create new spec files and fill them as you please


## Credit

* [Spec CSS](https://certif.com/content/spec/) for providing an open remote interface
* [Python](http://python.org) for being such a great programming language and community
* [numpy](http://www.numpy.org/) for handling arrays for us
* [gevent](http://www.gevent.org) for making concurrency not so hard to live with
* [blinker](https://pythonhosted.org/blinker/) for helping us handle events

[pipeline_badge]: https://gitlab.com/tiagocoutinho/pyspec/badges/master/pipeline.svg
[coverage_badge]: https://gitlab.com/tiagocoutinho/pyspec/badges/master/coverage.svg?job=pages

[version_badge]: https://img.shields.io/pypi/v/pyspec.svg
[status_badge]: https://img.shields.io/pypi/status/pyspec.svg
[python_badge]: https://img.shields.io/pypi/pyversions/pyspec.svg
[wheel_badge]: https://img.shields.io/pypi/wheel/pyspec.svg
[downloads_month_badge]: https://img.shields.io/pypi/dm/pyspec.svg
