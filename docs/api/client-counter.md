title: Counter API
description: Spec client Counter python API

Once you have an instance of a spec client object, you can ask for any of its
counters with:

```python
>>> sec = spec.get_counter('sec')
```

You should not try to instanciate the Counter class directly. The events may
not work as you expect.

## Concurrency model

!!! note
    The counter object you get from Spec will inherit its concurrency model object
    (see [spec concurrency models](/api/client-spec/#concurrency-models)).


## Properties

You can access any of the counter properties as python object properties:

```python
>>> sec.value
0.1
```

### Counter.name

*read only*

returns the counter name


### Counter.value

*read only*

returns the current value

## Events

Spec counter object provides signals which you can connect to to be informed of
asynchronous events. Currently, only the *value* property supports these kind of
events.

The interface is similar to spec:

```python

def cb(sender, event):
    print('{0} value: {1}'.format(event.channel, event.value))

sec.register('value', cb)
```

To unregister the callback simply call `th.unregister('value', cb)`.