# -*- coding: utf-8 -*-

# https://certif.com/spec_help/server.html

from __future__ import absolute_import

import sys
import struct
import logging
import datetime
import functools
import itertools
import collections

import numpy

from pyspec import util
from pyspec import signal


PORT_FIRST = 6510
PORT_LAST = 6530
PORT_RANGE = tuple(range(PORT_FIRST, PORT_LAST+1))
DEFAULT_HOST = 'localhost'
DEFAULT_PORT = PORT_FIRST
DEFAULT_ADDR = '{0}:{1}'.format(DEFAULT_HOST, DEFAULT_PORT)

MAGIC = 4277009102
VERSION = 4
NAME_LEN = 80

HEADER_PREFIX = '<IiI' # magic, version, size
HEADER_PREFIX_STRUCT = struct.Struct(HEADER_PREFIX)
HEADER_BASE = HEADER_PREFIX + 'IIIiiIII'
HEADER_BASE_STRUCT = struct.Struct(HEADER_BASE)
HEADER_SUFFIX = '{0}s'.format(NAME_LEN)
HEADER_FRAME = '{0}{{0}}{1}'.format(HEADER_BASE, HEADER_SUFFIX)

# Commands
CLOSE =            1     # From Client
ABORT =            2     # From Client
CMD =              3     # From Client
CMD_WITH_RETURN =  4     # From Client
RETURN =           5     # Not yet used
REGISTER =         6     # From Client
UNREGISTER =       7     # From Client
EVENT =            8     # From Server
FUNC =             9     # From Client
FUNC_WITH_RETURN = 10    # From Client
CHAN_READ =        11    # From Client
CHAN_SEND =        12    # From Client
REPLY =            13    # From Server
HELLO =            14    # From Client
HELLO_REPLY =      15    # From Server

CMD_NAMES = {
    CLOSE: 'CLOSE',
    ABORT: 'ABORT',
    CMD: 'CMD',
    CMD_WITH_RETURN: 'CMD_WITH_RETURN',
    RETURN: 'RETURN',
    REGISTER: 'REGISTER',
    UNREGISTER: 'UNREGISTER',
    EVENT: 'EVENT',
    FUNC: 'FUNC',
    FUNC_WITH_RETURN: 'FUNC_WITH_RETURN',
    CHAN_READ: 'CHAN_READ',
    CHAN_SEND: 'CHAN_SEND',
    REPLY: 'REPLY',
    HELLO: 'HELLO',
    HELLO_REPLY: 'HELLO_REPLY'
}

# Types
DOUBLE =      1
STRING =      2
ERROR =       3
ASSOC =       4
ARR_DOUBLE =  5
ARR_FLOAT =   6
ARR_LONG =    7
ARR_ULONG =   8
ARR_SHORT =   9
ARR_USHORT =  10
ARR_CHAR =    11
ARR_UCHAR =   12
ARR_STRING =  13
ARR_LONG64 =  14
ARR_ULONG64 = 15

TYPE_NAMES = {
    DOUBLE: 'DOUBLE',
    STRING: 'STRING',
    ERROR: 'ERROR',
    ASSOC: 'ASSOC',
    ARR_DOUBLE: 'DOUBLE[]',
    ARR_FLOAT: 'FLOAT[]',
    ARR_LONG: 'LONG[]',
    ARR_ULONG: 'ULONG[]',
    ARR_SHORT: 'SHORT[]',
    ARR_USHORT: 'USHORT[]',
    ARR_CHAR: 'CHAR[]',
    ARR_UCHAR: 'UCHAR[]',
    ARR_STRING: 'STRING[]',
    ARR_LONG64: 'LONG64[]',
    ARR_ULONG64: 'ULONG64[]'
}

# Flags
DELETED = 0x1000

NULL = b'\x00'
ASSOC_SEP = b'\x1c'
ASSOC_SEP_STR = util.ensure_str(ASSOC_SEP)


SPEC_TYPE_MAP = {
    ARR_CHAR: numpy.byte,
    ARR_UCHAR: numpy.ubyte,
    ARR_SHORT: numpy.int16,
    ARR_USHORT: numpy.uint16,
    ARR_LONG: numpy.int32,
    ARR_ULONG: numpy.uint32,
    ARR_LONG64: numpy.int64,
    ARR_ULONG64: numpy.uint64,
    ARR_FLOAT: numpy.float32,
    ARR_DOUBLE: numpy.float64,
}
DTYPE_MAP = {v: k for k, v in SPEC_TYPE_MAP.items()}
# add numpy.dtype('float64') and the like
DTYPE_MAP.update({k().dtype: v for k, v in DTYPE_MAP.items()})


class SpecError(Exception):
    pass


class HeaderStruct(struct.Struct):
    def __init__(self, version, specific='', template = HEADER_FRAME):
        super(HeaderStruct, self).__init__(template.format(specific))
        self.version = version
        self.template = template
        self.specific = specific


HEADER_VERSIONS = {
    2: HeaderStruct(2),
    3: HeaderStruct(3, 'i'),
    4: HeaderStruct(4, 'ii'),
}


HEADER_HIGHEST = HEADER_VERSIONS[max(*HEADER_VERSIONS.keys())]

Header = collections.namedtuple('Header', ('version', 'size', 'serial_number',
                                           'time', 'command', 'type', 'rows',
                                           'cols', 'len', 'error', 'flags',
                                           'name', 'unknown'))


Message = collections.namedtuple('Message', ('header', 'payload'))


_new_message_id = functools.partial(next, itertools.count(1))


def get_header_struct(data):
    magic, version, size = HEADER_PREFIX_STRUCT.unpack_from(data)
    assert magic == MAGIC
    assert version >= 2
    known_version = version in HEADER_VERSIONS
    if known_version:
        header_struct = HEADER_VERSIONS[version]
    else:
        unknown_size = size - HEADER_HIGHEST.size
        specific = HEADER_HIGHEST.specific + '{}s'.format(unknown_size)
        header_struct = HeaderStruct(version, specific)
    return header_struct


def unpack_header(data, header_struct=None):
    """Decode the data header into a Header tuple"""
    if header_struct is None:
        header_struct = get_header_struct(data)
    version, size = header_struct.version, header_struct.size
    fields = header_struct.unpack_from(data)
    time = datetime.datetime.fromtimestamp(fields[4] + fields[5]*1e-6)
    cmd_type = fields[7]
    err = fields[11] if version > 2 else None
    flags = fields[12] if version > 3 else None
    name = fields[-1]
    name = name[:name.find(NULL)].decode()
    unknown = b'' if version in HEADER_VERSIONS else fields[-2]
    return Header(version, size, fields[3], time, fields[6], cmd_type,
                  fields[8], fields[9], fields[10], err, flags, name,
                  unknown)


def pack_header(header):
    version = header.version
    header_struct = HEADER_VERSIONS[version]
    ts = header.time if header.time else datetime.datetime.now()
    sec = int((ts - datetime.datetime.fromtimestamp(0)).total_seconds())
    usec = int(ts.microsecond)
    args = [MAGIC, header.version, header_struct.size, header.serial_number,
            sec, usec, header.command, header.type, header.rows, header.cols,
            header.len]
    if version > 2:
        args.append(header.error)
    if version > 3:
        args.append(header.flags)
    args.append(header.name.encode())
    return header_struct.pack(*args)


def decode_payload_item(data, dtype=STRING):
    if dtype == STRING:
        try:
            return int(data)
        except ValueError:
            try:
                return float(data)
            except ValueError:
                pass
    return util.ensure_str(data)


def decode_payload_assoc(payload):
    fields = payload.split(NULL)
    keys = map(util.ensure_str, fields[::2])
    values = map(decode_payload_item, fields[1::2])
    result = {}
    for key, value in zip(keys, values):
        present = key in result
        compound = ASSOC_SEP_STR in key
        if compound:
            key, k2 = key.split(ASSOC_SEP_STR)
            v1 = result.get(key)
            if v1 is None:
                v1 = {}
            elif not isinstance(v1, dict):
                v1 = {None: v1}
            v1[k2] = value
            value = v1
        if present and not compound:
            result[key][None] = value
        else:
            result[key] = value
    return result


def decode_payload_array_numpy(header, payload):
    dtype = SPEC_TYPE_MAP[header.type]
    array = numpy.frombuffer(payload, dtype=dtype)
    array.shape = header.rows, header.cols
    return array


def decode_payload_array(header, payload):
    if header.type == ARR_STRING:
        result = []
        cols = header.cols
        for i in range(header.rows):
            item = payload[i*cols:(i+1)*cols]
            null = item.find(NULL)
            if null >= 0:
                item = item[:null]
            result.append(util.ensure_str(item))
        return result
    else:
        return decode_payload_array_numpy(header, payload)


def decode_payload(header, payload):
    if not payload:
        return None
    spec_type = header.type
    if spec_type == ERROR:
        return util.ensure_str(payload)
    elif spec_type == STRING:
        # get rid of ending '\x00'
        payload = payload[:-1]
        return decode_payload_item(payload)
    elif spec_type == ASSOC:
        # get rid of ending '\x00\x00'
        payload = payload[:-2]
        return decode_payload_assoc(payload)
    else:
        return decode_payload_array(header, payload)


def encode_simple(value):
    if value is None:
        return STRING, 0, 0, b''
    elif util.is_str_or_bytes(value):
        return STRING, 0, 0, util.ensure_binary(value) + NULL
    elif isinstance(value, (int, float)):
        return STRING, 0, 0, util.ensure_binary(str(value)) + NULL
    elif isinstance(value, bool):
        return STRING, 0, 0, b'1' if value else b'0' + NULL
    raise ValueError('Not a simple type')


def encode_assoc(obj, prefix=b''):
    result = []
    for key, value in obj.items():
        key = b'' if key is None else util.ensure_binary(key)
        if util.is_map(value):
            result.append(encode_assoc(value, prefix=key+ASSOC_SEP)[-1])
        else:
            if prefix:
                key = prefix + key if key else prefix[:-1]
            value = encode_simple(value)[-1]
            result.append(key + NULL + value)
    if not prefix:
        result.append(NULL)
    return ASSOC, 0, 0, b''.join(result)


def encode_numpy(value):
    if value.ndim > 2:
        raise ValueError('Cannot send array with more than two dimensions')
    result = value.tobytes()
    spec_type = DTYPE_MAP[value.dtype]
    rows = value.shape[0]
    cols = value.shape[1] if value.ndim > 1 else 1
    return spec_type, rows, cols, result


def encode_array_string(value):
    rows = len(value)
    cols = max(map(len, value))
    result = bytearray(rows*cols)
    for i, v in enumerate(value):
        result[i*cols:i*cols+len(v)] = util.ensure_binary(v)
    return ARR_STRING, rows, cols, result


def encode_payload(value, error=False):
    if error:
        return ERROR, 0, 0, util.ensure_binary(value)
    try:
        return encode_simple(value)
    except ValueError:
        pass
    if util.is_map(value):
        return encode_assoc(value)
    elif isinstance(value, numpy.ndarray):
        return encode_numpy(value)
    elif util.is_sequence(value):
        if value and util.is_str_or_bytes(value[0]):
            return encode_array_string(value)
        value = numpy.array(value)
        if value.ndim == 1:
            value.shape = 1, value.shape[0]
        return encode_numpy(value)
    raise ValueError('Cannot encode value')


def encode_command(cmd, *args, **options):
    """Convert a command list to a Spec command string"""
    cmd_list = [util.ensure_binary(cmd)]
    for arg in args:
        arg_str = str(arg)
        if isinstance(arg, dict):
            arg_str = arg_str.replace('{', '[').replace('}', ']')
        cmd_list.append(util.ensure_binary(arg_str))
    separator = options.get('separator', NULL)
    return separator.join(cmd_list)


def unpack_message(data):
    """Decode the data into a Message tuple"""
    header = unpack_header(data)
    return Message(header, data[header.size:])


def pack_message(message):
    return pack_header(message.header) + message.payload


def create_message(command, name='', version=VERSION, payload=b'',
                   serial_number=None, error=False):
    header_struct = HEADER_VERSIONS[version]
    if serial_number is None:
        serial_number = _new_message_id()
    spec_type, rows, cols, payload = encode_payload(payload, error=error)
    header = Header(version, header_struct.size, serial_number,
                    datetime.datetime.now(), command, spec_type,
                    rows=rows, cols=cols,
                    len=len(payload), error=0, flags=0, name=name, unknown=b'')
    return Message(header, payload)


def hello_message():
    return create_message(HELLO)


def readn(sock, n):
    # low level basic socket read. Note that we are not handling the case of
    # receiving less bytes than asked. This will be an improvement... So TODO
    data = sock.read(n)
    if not data:
         raise util.ConnectionError('remote end disconnected')
    assert len(data) == n
    return data


def read_message(sock, header_struct=None):
    if header_struct is None:
        n = HEADER_BASE_STRUCT.size
        header_bytes = readn(sock, n)
        header_struct = get_header_struct(header_bytes)
        header_bytes += readn(sock, header_struct.size - n)
    else:
        header_bytes = readn(sock, header_struct.size)
    header = unpack_header(header_bytes, header_struct)
    if header.len:
        payload = readn(sock, header.len)
        payload = decode_payload(header, payload)
    else:
        payload = None
    message = Message(header, payload)
    return message, header_struct


def write_message(sock, message):
    data = pack_message(message)
    return sock.write(data)


def write_messages(sock, messages):
    data = b''.join(pack_message(message) for message in messages)
    return sock.write(data)


def raw_write_read_message(sock, request):
    """
    Assumes no event will happen between a write and read.
    Naive but useful when discovering spec sessions
    """
    write_message(sock, request)
    return read_message(sock)[0]


def get_io(io):
    if io is None:
        from pyspec.network import thread as io
    if util.is_str_or_bytes(io):
        pack = 'pyspec.network.' + io
        __import__(pack)
        return sys.modules[pack]
    return io


def name_2_addr(name=DEFAULT_ADDR):
    if not name:
        host, port = DEFAULT_HOST, DEFAULT_PORT
    elif ':' in name:
        host, port = name.split(':')
    else:
        host, port = 'localhost', name
    if not host:
        host = '0'
    try:
        port = int(port)
    except ValueError:
        pass
    return host, port


def find_spec(name=DEFAULT_ADDR, io=None, ports=PORT_RANGE):
    io = get_io(io)
    host, port = name_2_addr(name)
    if isinstance(port, str):
        message = hello_message()
        for tmp_port in ports:
            try:
                sock = io.Socket(host, tmp_port)
                reply = raw_write_read_message(sock, message)
                if port == reply.payload:
                    return sock
                sock.close()
            except Exception as e:
                continue
        raise ValueError('Cannot find spec {0!r}'.format(name))
    else:
        return io.Socket(host, port)


def Transport(name, io=None):
    io = get_io(io)
    host, port = name_2_addr(name)
    def accept(transport):
        reply = raw_write_read_message(transport, hello_message())
        return reply.payload == port if isinstance(port, str) else True
    return io.Socket(host, port, accept)


class Protocol(object):

    def __init__(self, name=DEFAULT_ADDR, io=None):
        self._log = logging.getLogger(type(self).__name__)
        self._addr = name
        self._name = None
        self.io = get_io(io)
        self.transport = Transport(name=name, io=self.io)
        self.header_struct = None
        self.pending = {}
        self.on = signal.On()
        self.on.event_received = signal.Signal()
        self.on.connected = self.transport.on.connected
        self.on.disconnected = self.transport.on.disconnected
        self.transport.on.disconnected.connect(self._on_disconnect)
        self.transport.on.data.connect(self._on_data)

    def __repr__(self):
        return 'Protocol({0})'.format(self._addr)

    def create_message(self, command, name='', payload=b''):
        if self.header_struct is None:
            # force a request which initiates the header_struct
            self._name = self.hello()
        return create_message(command=command, name=name,
                              version=self.header_struct.version,
                              payload=payload)

    @property
    def name(self):
        if self._name is None:
            self._name = self.hello()
        return self._name

    @property
    def full_name(self):
        try:
            name = self.name
        except:
            name = 'spec'
        return '{0!r} @ {1}:{2}'.format(name, self.transport.host,
                                        self.transport.port)

    @property
    def connected(self):
        return self.transport.connected

    def disconnect(self):
        self.transport.disconnect()

    def close(self):
        self.disconnect()

    def read_message(self):
        msg, header_struct = read_message(self.transport, self.header_struct)
        self._log.debug('read %s', msg)
        self.header_struct = header_struct
        return msg

    def write_message(self, message):
        self._log.debug('write %s', message)
        return write_message(self.transport, message)

    def write_messages(self, messages):
        return write_messages(self.transport, messages)

    def _register_message(self, message):
        future = self.io.Future()
        self.pending[message.header.serial_number] = future
        return future

    def _get_reply_future(self, message):
        return self.pending[message.header.serial_number]

    def _get_reply(self, message, timeout=None):
        return self._get_reply_future(message).result(timeout)

    def _discard_reply(self, message):
        return self.pending.pop(message.header.serial_number, None)

    def _on_disconnect(self, transport):
        for future in self.pending.values():
            future.set_exception(util.ConnectionError('remote end disconnected'))
        self.pending.clear()

    def _on_data(self, transport):
        try:
            message = self.read_message()
        except util.ConnectionError:
            return
        if message.header.command == EVENT:
            self.on.event_received.send(self, message=message)
            return
        future = self.pending.pop(message.header.serial_number, None)
        err = message.header.type == ERROR

        if future is None:
            log = self._log.warning if err else self._log.info
            err_msg = 'error ' if err else ''
            log('received spurious {}message [name=%r cmd=%s type=%s]: %s',
                err_msg,
                message.header.name,
                CMD_NAMES[message.header.command],
                TYPE_NAMES[message.header.type],
                message.payload)
        else:
            if err:
                future.set_exception(SpecError(message.payload))
            else:
                future.set_result(message.payload)

    def write_read_message(self, message, wait=True, timeout=None):
        reply_future = self._register_message(message)
        try:
            self.write_message(message)
        except:
            self._discard_reply(message)
            util.reraise(*sys.exc_info())
        return reply_future.result(timeout=timeout) if wait else reply_future

    def read_channel(self, channel, wait=True, timeout=None):
        request = self.create_message(CHAN_READ, name=channel)
        return self.write_read_message(request, wait=wait, timeout=timeout)

    def write_channel(self, channel, value=None):
        payload = b'' if value is None else value
        request = self.create_message(CHAN_SEND, name=channel, payload=payload)
        reply = self.write_message(request)

    def register_channel(self, channel):
        self._log.debug('register events for %s', channel)
        request = self.create_message(REGISTER, name=channel)
        self.write_message(request)

    def unregister_channel(self, channel):
        self._log.debug('unregister events for %s', channel)
        request = self.create_message(UNREGISTER, name=channel)
        self.write_message(request)

    def hello(self):
        request = hello_message()
        reply = self.write_read_message(request)
        return reply

    def abort(self):
        request = self.create_message(ABORT)
        self.write_message(request)

    def function(self, cmd, *args, **options):
        payload = encode_command(cmd, *args, separator=NULL)
        request = self.create_message(FUNC_WITH_RETURN, payload=payload)
        return self.write_read_message(request, **options)

    def command(self, cmd, *args, **options):
        payload = encode_command(cmd, *args, separator=b' ')
        request = self.create_message(CMD_WITH_RETURN, payload=payload)
        return self.write_read_message(request, **options)

    def start_move(self, *args):
        msgs = [self.create_message(CHAN_SEND, name='motor/../prestart_all')]
        for motor, pos in zip(args[::2], args[1::2]):
            channel = 'motor/{}/start_one'.format(motor)
            msg = self.create_message(CHAN_SEND, name=channel,
                                      payload=pos)
            msgs.append(msg)
        msgs.append(self.create_message(CHAN_SEND, name='motor/../start_all'))
        self.write_messages(msgs)

    def start_count(self, value):
        self.write_channel('scaler/.all./count', value)

