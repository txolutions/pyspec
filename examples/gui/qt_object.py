
import sys
import pyspec

try:
    from PySide2.QtCore import QObject, Signal, Slot
    from PySide2.QtWidgets import (QApplication, QWidget, QVBoxLayout,
                               QLabel, QPushButton)
except ImportError:
    from PySide.QtCore import QObject, Signal, Slot
    from PySide.QtGui import (QApplication, QWidget, QVBoxLayout,
                               QLabel, QPushButton)


class QSpec(QObject):
    ready = Signal(object)

@Slot(int)
def on_ready(event):
    if event.type == event.DISCONNECTED:
        text = 'disconnected' 
        color = '#e0e0e0'
        button.setEnabled(False)
    else:
        if event.value: 
            text = 'ready!' 
            color = '#60f060'
            button.setEnabled(True)
        else: 
            text = 'busy...'
            color = '#f0f060'
            button.setEnabled(False)
    label.setText('spec is: ' + text)
    label.setStyleSheet('background-color: %s' % color)

def spec_sleep():
    spec.run('print "sleeping 5 secs...";sleep(5);print "done"', wait=False)

app = QApplication(sys.argv)
panel = QWidget()
l = QVBoxLayout(panel)
label = QLabel("Spec status:")
button = QPushButton('Give work to spec')
l.addWidget(label)
l.addWidget(button)
panel.show()

spec = pyspec.Spec('fourc')
qspec = QSpec()
cb = lambda sender, event: qspec.ready.emit(event)
spec.register_status('ready', cb)

qspec.ready.connect(on_ready)
button.clicked.connect(spec_sleep)
sys.exit(app.exec_())

