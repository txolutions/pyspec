# Getting started

## Installation

PySpec is a pure python package. From your terminal of choice, and
within your favorite python virtual environment simply type:

    $ pip install pyspec

This will install the latest version of pyspec.

pyspec comes with an optional pyspec server. To install it simply type:

    $ pip install pyspec[server]

## Quick start

Eager to get started? Here you will find a short introduction.

```python
>>> # start by importing pyspec
>>> import pyspec
```

### Spec client

Connect to Spec running on your computer (make sure Spec is
running in [server mode](https://certif.com/spec_help/server.html)):

```python
>>> spec = pyspec.Spec()
```

By default pyspec connects to spec on port `6510`. Check the
[details](/api/client-spec) to see how to connect to a specific port,
a specific session or a remote session running on another machine.

You can check the session name or spec version simply with:

```python
>>> spec.name
'fourc'
>>> spec.version
'6.06.10'
```

#### Variables

```python
>>> spec.get('VERSION')
'6.06.10'
>>> spec.get('USER')
'guido'

>>> # spec associative arrays are python dict
>>> spec.get('tomo_config')
{'axes': {'main_motor': 'th', 'secondary': 'chi'},
 'mode': 'helicoidal',
 'ac_mode': 'time'}

>>> # spec arrays are numpy arrays
>>> spec.get('arr_long')
array([[0, 1, 2, 3, 4, 5]])
```

#### Motors

```python
>>> th = spec.get_motor('th')
>>> th.position
23.45
>>> th.offset
-0.1345
>>> # blocking move
>>> th.move(90)

>>> # non-blocking move
>>> future = th.move(30, wait=False)

>>> # check if running
>>> future.running()
True

>>> # wait for motion to finish
>>> future.result()

>>> # all pyspec objects will give you human readable
>>> # information (repr) about themselves:
>>> th
Motor(th)
```

Moving multiple motors is done though the spec object:

```
>>> spec.move('th', 10, 'chi', 30)

>>> # non-blocking version:
>>> future = spec.move('th', 5, 'chi', 20, wait=False)
>>> # wait for motion to finish with:
>>> future.result()

```

### Counters

```python
>>> sec = spec.get_counter('sec')
>>> sec.value
0.0

>>> # counting is done at the spec level
>>> spec.count(0.9)
>>> sec.value
0.9

>>> # non blocking version:
>>> future = spec.count(1.2)
>>> spec.is_counting
True
>>> sec.value
0.1754
>>> # wait for counting to finish with:
>>> future.result()
>>> sec.value
1.2
>>> spec.is_counting
False
```

### Spec file

[Spec file](https://certif.com/spec_help/scans.html) manipulation:

#### Reading

```python
>>> import pyspec.file

>>> # open a spec file in read mode
>>> sf = pyspec.file.SpecFile('mydata.dat')
# how many scans are in the file
>>> len(sf)
35
# access a specific scan
>>> scan0 = sf[0]
>>> scan0
Scan(cmd='ascan th 0 90 1000 0.2', nb=1, id='1.1')

>>> scan0.command
'ascan th 0 90 1000 0.2'

>>> scan0.date
'Wed Feb 14 14:37:55 2018'

>>> s0.labels
['Time',
 'Epoch',
 'Seconds',
 'pview',
 'setpt',
 'wsetpt',
 'op',
 'pview2',
 'Monitor',
 'Detector']
>>> scan0.data
array([[43.96200917, 91.71704562, 54.84654099, ..., 69.89079393,
        41.30574782, 10.60506855],
       [67.45270572, 99.68765189, 11.40850914, ..., 39.1102865 ,
        10.75778966, 35.44737546],
       [63.24368461, 68.86598968, 54.02818098, ..., 55.0911208 ,
        47.28626984, 98.54361212],
       ...,
       [85.74934881, 47.51319073, 63.29038048, ..., 85.35711446,
        91.08667859, 65.91230248],
       [ 7.01237153, 52.61849809, 10.33106001, ..., 30.79810938,
         7.05284899, 12.85134087],
       [28.37719983, 46.76271604, 48.90722539, ..., 21.88906115,
        10.57748534, 70.09580593]])
```

#### Writting

You can also create a completely new spec file with:

```python

>>> # create a new file or overwrite an existing one
>>> # (use mode='a' to modify an existing file)
>>> sf = pyspec.file.SpecFile('mydata.dat', mode='w')

>>> # give a session name
>>> # (user is by default the logged in user, but you can change it)
>>> sf.header.title = 'exp1'

>>> # you can preview what the file contents will look like with:
>>> print(sf)
#F mydata.dat
#E 1553362827
#D Sat Mar 23 18:40:27 2019
#C exp1  User = coutinho

>>> # add some motors
>>> sf.header.motor_names = ['Theta', 'Chi']
>>> sf.header.motor_mnemonics = ['th', 'chi']

>>> # add a scan (don't create a Scan object directly)
>>> scan0 = sf.add_scan(nb=55, command="my_special_scan th 0 90 1000")

>>> # fill scan information
>>> scan0.labels = ['Time', 'Epoch', 'C1', 'Temp']
>>> scan0.motor_positions = [60, 30]
>>> # fill in scan data
>>> scan0.data = [[0.01, 3.5, -66.4, 45],
                  [0.02, 3.8, -5.32, 46]]

>>> # by now your file should look something like this
>>> print(sf)
#F mydata.dat
#E 1553362827
#D Sat Mar 23 18:40:27 2019
#C exp1  User = coutinho
#O0 Theta  Chi
#o0 th chi

#S 55  my_special_scan th 0 90 1000
#D Sat Mar 23 18:53:53 2019
#P0 60 30
#N 4
#L Time  Epoch  C1  Temp
0.01 3.5 -66.4 45.0
0.02 3.8 -5.32 46.0

>>> # now might be a good time to store the file persistently
>>> sf.save()
```