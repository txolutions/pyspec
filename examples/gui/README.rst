
This directory contains examples showing how to connect a graphical user interface with Qt and spec events in server mode

The files in this directory are:

qtspecwidget.py
    this file implements a widget that will show the spec ready/busy state. A button in the widget allows
    to run a series of commands/macros in spec
 
    the widget is initialized with the name of the 'spec' application as a parameters as well as the type     of 'io' concurrency used ('gevent' or 'thread')

qt_thread_example.py 
    uses qtspecwidget.py and uses a thread model for concurrency

qt_gevent_example.py
    uses qtspecwidget.py and uses greenlet (gevent) model for concurrency
    gevent model needs to write a hook for updating greenlets during the main Qt loop.
    On the contrary the gevent concurrenty model has a lighter footprint as everything runs on the same execution space. Gevent model is well adapted for routines spending time waiting in i/o calls, waiting for subproceses or other.
    For intense CPU routines, use a thread model instead.

qt_object.py  - 
    connection is done though a non-gui QObject object that receives spec signal and re-emits Qt signals

