# -*- coding: utf-8 -*-

import struct
import datetime
import contextlib

from pyspec import protocol

import pytest


MAGIC = 4277009102
V2 = struct.Struct('<IiIIIIiiIII80s')
V3 = struct.Struct('<IiIIIIiiIIIi80s')
V4 = struct.Struct('<IiIIIIiiIIIii80s')
V99 = struct.Struct('<IiIIIIiiIIIiiii80s')

HELLO = 14
HELLO_REPLY = 15

pack_headers = {
    'V2': (V2.pack(MAGIC, 2, 124, 55, 2, 3, 123, 456, 5, 6, 0, b'hello'),
           protocol.Header(version=2, size=124, serial_number=55,
                           time=datetime.datetime.fromtimestamp(2+3e-6),
                           command=123, type=456,
                           rows=5, cols=6, len=0, error=None,
                           flags=None, name='hello', unknown=b'')),
    'V3': (V3.pack(MAGIC, 3, 128, 56, 2, 3, 123, 456, 5, 6, 0, 0, b'hello'),
           protocol.Header(version=3, size=128, serial_number=56,
                           time=datetime.datetime.fromtimestamp(2+3e-6),
                           command=123, type=456,
                           rows=5, cols=6, len=0, error=0,
                           flags=None, name='hello', unknown=b'')),
    'V4': (V4.pack(MAGIC, 4, 132, 57, 2, 3, 123, 456, 5, 6, 0, 0, 0, b'hello'),
           protocol.Header(version=4, size=132, serial_number=57,
                           time=datetime.datetime.fromtimestamp(2+3e-6),
                           command=123, type=456,
                           rows=5, cols=6, len=0, error=0,
                           flags=0, name='hello', unknown=b'')),
}

unpack_headers = dict(pack_headers,
    V99=(V99.pack(MAGIC, 99, 140, 57, 2, 3, 123, 456, 5, 6, 0, 0, 0, 1299, 588, b'hello'),
         protocol.Header(version=99, size=140, serial_number=57,
                         time=datetime.datetime.fromtimestamp(2+3e-6),
                         command=123, type=456,
                         rows=5, cols=6, len=0, error=0,
                         flags=0, name='hello', unknown=b'\x13\x05\x00\x00L\x02\x00\x00')),
)

IOS = 'io', ['gevent', 'thread']


def test_base():
    assert V2.size == 124
    assert V3.size == 128
    assert V4.size == 132


@pytest.mark.parametrize('message, expected_header',
                         tuple(unpack_headers.values()),
                         ids=tuple(unpack_headers.keys()))
def test_unpack_header(message, expected_header):
    assert protocol.unpack_header(message) == expected_header


@pytest.mark.parametrize('expected_message, header',
                         tuple(pack_headers.values()),
                         ids=tuple(pack_headers.keys()))
def test_pack_header(expected_message, header):
    assert protocol.pack_header(header) == expected_message


def assert_message_identical_without_timestamp(msg1, msg2):
    assert isinstance(msg1, bytes)
    assert isinstance(msg2, bytes)
    assert len(msg1) == len(msg2)
    assert msg1[:16] == msg1[:16]
    assert msg1[24:] == msg2[24:]


def test_hello_message_request():
    expected_bytes = V4.pack(MAGIC, 4, 132, 57, 2, 3, HELLO,
                             2, 0, 0, 1, 0, 0, b'') + b'\x00'
    msg1 = protocol.create_message(protocol.HELLO)
    bytes1 = protocol.pack_message(msg1)
    msg2 = protocol.hello_message()
    bytes2 = protocol.pack_message(msg2)
    assert_message_identical_without_timestamp(bytes1, expected_bytes)
    assert_message_identical_without_timestamp(bytes2, expected_bytes)


@pytest.mark.parametrize(*IOS)
def test_find_spec(spec_server, io):
    port = spec_server.port
    name = spec_server.config.name
    no_name = '__weirdspec__'
    assert no_name != name
    assert port not in protocol.PORT_RANGE

    with pytest.raises(ValueError):
        with contextlib.closing(protocol.find_spec(no_name, io=io)):
            pass

    with pytest.raises(ValueError):
        with contextlib.closing(protocol.find_spec(name, io=io)):
            pass

    with pytest.raises(ValueError):
        with contextlib.closing(protocol.find_spec(name, io=io,
                                                   ports=protocol.PORT_RANGE)):
            pass

    with contextlib.closing(protocol.find_spec(spec_server.address, io=io)) as sock:
        assert sock.host == 'localhost'
        assert sock.port == port

    prange = range(port-10, port+10)
    with contextlib.closing(protocol.find_spec(name, io=io, ports=prange)) as sock:
        assert sock.host == 'localhost'
        assert sock.port == port


@pytest.mark.parametrize(*IOS)
def test_protocol_creation(spec_server, io):
    port = spec_server.port
    name = spec_server.config.name
    no_name = '__weirdspec__'
    assert no_name != name
    assert port not in protocol.PORT_RANGE

    with contextlib.closing(protocol.Protocol(no_name, io=io)):
        pass

    with contextlib.closing(protocol.Protocol(spec_server.address, io=io)) as proto:
        assert proto.transport.host == 'localhost'
        assert proto.transport.port == port


@pytest.mark.parametrize(*IOS)
def test_protocol(spec_server, io):
    cfg = spec_server.config
    address = spec_server.address
    proto = protocol.Protocol(address, io=io)
    assert repr(proto) == 'Protocol({0})'.format(spec_server.address)
    assert not proto.connected
    assert proto.name == cfg.name
    assert proto.connected
    assert proto.full_name == '{0!r} @ {1}'.format(cfg.name,
                                                   address)

    assert proto.function("pow", 3, 6) == 729
