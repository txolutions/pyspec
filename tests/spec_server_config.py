name = "__pyspec__"
version = "6.06.09"
bind = "localhost:64103"


def A1(dtype):
    import numpy
    data = numpy.linspace(99.3, 102, 56860).astype(dtype)
    data.shape = 1, data.shape[0]
    return data


def A2(dtype):
    import numpy
    data = numpy.linspace(99.3, 102, 56860)
    return data.reshape((-1, 10)).astype(dtype)


variables = dict(
    simple_var="hello",
    tomo=dict(
        active_motor='chi',
        detector='pilatus1',
        counter='P201_1',
        nint=5,
        nfloat=-56.76
    ),
    super_tomo=dict(
        hello='world',
        data={None:'default',
              'dfloat': 55.5,
              'dint': -10,
              'dstr': 'something'}),
    arr_str_1d=['a', '1d', 'array', 'of strings in spec'],
    arr_char_1d=A1('int8'),
    arr_uchar_1d=A1('uint8'),
    arr_int16_1d=A1('int16'),
    arr_uint16_1d=A1('uint16'),
    arr_int32_1d=A1('int32'),
    arr_uint32_1d=A1('uint32'),
    arr_int64_1d=A1('int64'),
    arr_uint64_1d=A1('uint64'),
    arr_float32_1d=A1('float32'),
    arr_float64_1d=A1('float64'),
    arr_char_2d=A2('int8'),
    arr_uchar_2d=A2('uint8'),
    arr_int16_2d=A2('int16'),
    arr_uint16_2d=A2('uint16'),
    arr_int32_2d=A2('int32'),
    arr_uint32_2d=A2('uint32'),
    arr_int64_2d=A2('int64'),
    arr_uint64_2d=A2('uint64'),
    arr_float32_2d=A2('float32'),
    arr_float64_2d=A2('float64'),
    list_int_1d=[100,-10,400],
    list_float_1d=[-56.23, 590.43]
)

del A1, A2

motors = [
    dict(name='th', hard_low_limit=-180, hard_high_limit=180),
    dict(name='tth', hard_low_limit=-180, hard_high_limit=180),
    dict(name='chi', hard_low_limit=-180, hard_high_limit=180),
    dict(name='phi', hard_low_limit=-180, hard_high_limit=180),
]

counters = [
    dict(name="sec", type="SOFTWARE_TIMER")
]
