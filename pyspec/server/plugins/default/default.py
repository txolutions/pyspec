import time
import gevent
from pyspec.server.plugin import Plugin
from pyspec.server.plugins.default.motion import Motor

MOTION_EVENT_FREQUENCY = 0.2


class Counter(object):

    def __init__(self, name, config):
        self.name = name
        self.config = config
        assert config['type'] == 'SOFTWARE_TIMER'
        self._start = None
        self._acq = None
        self._value = 0

    def _finish(self, data):
        self._acq = None
        self._value = data

    def start(self, data):
        self._start = time.time()
        self._acq = gevent.spawn_later(data, self._finish, data)

    def stop(self):
        if self._acq:
            self._acq.kill()
            self._finish(time.time() - self._start)

    @property
    def counting(self):
        return self._acq is not None

    @property
    def value(self):
        return time.time() - self._start if self.counting else self._value


def compile_command(command):
    try:
        code = compile(command, '<string>', 'eval'), 'eval'
    except SyntaxError:
        code = compile(command, '<string>', 'exec'), 'exec'
    return code


class Tick:
    def __init__(self, plugin, period=1):
        self.plugin = plugin
        self.period = period
        self.task = None

    def start(self):
        gevent.spawn(self.run)

    def stop(self):
        if self.task:
            self.task.kill()
            self.task = None

    def run(self):
        i, t0 = 0, time.time()
        while True:
            nap = ((i+1)*self.period + t0) - time.time()
            gevent.sleep(nap)
            tick = self.plugin.get_variable('TICK')
            self.plugin.set_variable('TICK', tick+1)
            i += 1


class Default(Plugin):

    def __init__(self, spec):
        super(Default, self).__init__(spec)
        namespace = dict(self.config.get('variables', {}))
        namespace.setdefault('SPEC', spec.name)
        namespace.setdefault('VERSION', spec.config['version'])
        self.namespace = namespace
        self.motors = {}
        for motor_config in self.config.get('motors', []):
            motor_name = motor_config['name']
            self.motors[motor_name] = Motor(motor_name, motor_config)
        self.counters = {}
        for counter_config in self.config.get('counters', []):
            counter_name = counter_config['name']
            self.counters[counter_name] = Counter(counter_name, counter_config)
        self.acquisition = None
        self.motion_info = None
        self.motions = set()
        self.running = False
        if self.config.get('tick', False):
            self.tick = Tick(self)
            self.tick.start()
        else:
            self.tick = None

    def get_status(self, flag):
        if flag == 'ready':
            return 0 if self.running else 1
        elif flag == 'simulate':
            return 0
        elif flag == 'quit':
            return 0
        elif flag == 'shell':
            return 0

    def get_variable(self, var_name):
        return self.namespace[var_name]

    def set_variable(self, var_name, value):
        # provoke error in case variable does not exist
        self.namespace[var_name]
        self.namespace[var_name] = value
        self.send_var(var_name, value)

    def get_scaler_property(self, scaler_name, prop):
        if scaler_name == '.all.':
            if prop == 'count':
                return 0 if self.acquisition is None else 1
        else:
            scaler = self.counters[scaler_name]
            if prop == 'value':
                return scaler.value
        raise KeyError('scaler {0!r} has no property named {1!r}' \
                       .format(scaler_name, prop))

    def get_motor_property(self, motor_name, prop):
        motor = self.motors[motor_name]
        try:
            return getattr(motor, prop)
        except AttributeError:
            raise KeyError('motor {0!r} has no property named {1!r}' \
                           .format(motor_name, prop))

    def set_motor_property(self, motor_name, prop, value):
        # provoke error in case property does not exist
        self.get_motor_property(motor_name, prop)
        motor = self.motors[motor_name]
        setattr(motor, prop, value)
        self.send_motor(motor_name, prop, value)

    def prestart_all(self):
        self.motion_info = {}

    def start_all(self):
        self._start_motion()

    def abort_all(self):
        if self.motion_info:
            for motor in self.motion_info:
                motor.abort()

    def start_one(self, motor_name, value):
        motor = self.motors[motor_name]
        if self.motion_info is None:
            self.motion_info = {motor: value}
            self._start_motion()
        else:
            self.motion_info[motor] = value

    def _start_motion(self):
        task = gevent.spawn(self._do_motion, dict(self.motion_info))
        self.motion_info = None
        task.link(self._motion_finished)
        self.motions.add(task)

    def _send_motors_positions(self, motors):
        for motor in motors:
            dial, position = motor.dial_position, motor.position
            self.send_motor(motor.name, 'dial_position', dial)
            self.send_motor(motor.name, 'position', position)

    def _send_motors_move_done(self, motors, move_done):
        for motor in motors:
            self.send_motor(motor.name, 'move_done', move_done)

    def _do_motion(self, motion_info):
        self.log.info('start motion %s', motion_info)
        self._send_motors_move_done(motion_info, 1)
        try:
            self._send_motors_positions(motion_info)
            for motor, position in motion_info.items():
                motor.start_motion(position)
            last_event = time.time()
            while any([motor.moving for motor in motion_info]):
                t = time.time()
                if t - last_event > MOTION_EVENT_FREQUENCY:
                    self._send_motors_positions(motion_info)
                    last_event = t
                gevent.sleep(0.1)
            self._send_motors_positions(motion_info)
        finally:
            self._send_motors_move_done(motion_info, 0)
        self.log.info('end motion %s', motion_info)

    def _motion_finished(self, task):
        motion_info = self.motions.remove(task)

    def sync_check(self, motor_name):
        pass

    def run(self, command):
        self.send_status('ready', 0)
        try:
            code, mode = compile_command(command)
            if mode == 'eval':
                return eval(code, {}, self.namespace), False
            else:
                exec(code, {}, self.namespace)
                return None, False
        except Exception as e:
            return str(e), True
        finally:
            self.send_status('ready', 1)

    def start_count(self, value):
        self.acquisition = gevent.spawn(self._do_acquisition, value)
        self.acquisition.link(self._acquisition_finished)

    def stop_count(self):
        for counter in self.counters.values():
            counter.stop()

    def _send_counters_values(self, counters):
        for counter in counters:
            self.send_counter(counter.name, 'value', counter.value)

    def _send_counters_count(self, value):
        self.send_counter('.all.', 'count', value)

    def _do_acquisition(self, value):
        self.log.info('start acquisition %s', value)
        self._send_counters_count(1)
        counters = list(self.counters.values())
        try:
            for counter in counters:
                counter.start(value)
            self._send_counters_values(counters)
            last_event = time.time()
            while any([counter.counting for counter in counters]):
                t = time.time()
                if t - last_event > MOTION_EVENT_FREQUENCY:
                    self._send_counters_values(counters)
                    last_event = t
                gevent.sleep(0.1)
            self._send_counters_values(counters)
        finally:
            self._send_counters_count(0)
        self.log.info('end acquisition %s', value)

    def _acquisition_finished(self, task):
        self.acquisition = None
