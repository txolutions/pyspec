import logging

class RawPlugin(object):

    def __init__(self, spec):
        self.spec = spec
        self.log = logging.getLogger('{0}.{1}'.format(type(self).__name__,
                                                      spec.name))

    @property
    def config(self):
        return self.spec.config

    def get(self, name):
        raise NotImplementedError

    def set(self, name, value):
        raise NotImplementedError

    def run(self, command):
        raise NotImplementedError


class Plugin(RawPlugin):

    def get(self, name):
        if name.startswith('status/'):
            return self.get_status(name.rsplit('/', 1)[-1]), False
        elif name.startswith('var/'):
            try:
                return self.get_variable(name[4:]), False
            except KeyError:
                return b'no such var', True
        elif name.startswith('motor/'):
            _, motor_name, prop = name.rsplit('/', 2)
            try:
                return self.get_motor_property(motor_name, prop), False
            except KeyError:
                return b'motor property does not exist', True
        elif name.startswith('scaler/'):
            _, scalar_name, prop = name.rsplit('/', 2)
            return self.get_scaler_property(scalar_name, prop), False

    def set(self, name, value):
        if name.startswith('var/'):
            try:
                self.set_variable(name[4:], value)
                return None, False
            except KeyError:
                return b'no such var', True
        elif name.startswith('motor/'):
            _, motor_name, prop = name.rsplit('/', 2)
            try:
                if motor_name == '..':
                    getattr(self, prop)()
                    return None, False
                if prop in ('start_one', 'search', 'sync_check'):
                    getattr(self, prop)(motor_name, value)
                    return None, False
            except AttributeError:
                return b'motor property does not exist', True
            try:
                self.set_motor_property(motor_name, prop, value)
                return None, False
            except KeyError:
                return b'motor property does not exist', True
        elif name.startswith('scaler/'):
            if name == 'scaler/.all./count':
                if value:
                    self.start_count(value)
                    return None, False
                else:
                    self.stop_count()
                    return None, False

    def send(self, channel, value):
        self.spec.send(channel, value)

    def send_motor(self, name, prop, value):
        self.send('motor/{0}/{1}'.format(name, prop), value)

    def send_counter(self, name, prop, value):
        self.send('scaler/{0}/{1}'.format(name, prop), value)

    def send_var(self, name, value):
        self.send('var/{0}'.format(name), value)

    def send_status(self, name, value):
        self.send('status/{0}'.format(name), value)

    def get_status(self, channel):
        raise NotImplementedError


    def get_variable(self, name):
        raise NotImplementedError

    def set_variable(self, name, value):
        raise NotImplementedError


    def get_motor_property(self, motor_name, name):
        raise NotImplementedError

    def set_motor_property(self, motor_name, name, value):
        raise NotImplementedError

    def start_one(self, motor_name, value):
        raise NotImplementedError

    def prestart_all(self):
        raise NotImplementedError

    def start_all(self):
        raise NotImplementedError

    def abort_all(self):
        raise NotImplementedError

    def search(self, motor_name, value):
        raise NotImplementedError

    def sync_check(self, motor_name, value):
        raise NotImplementedError


    def get_scaler_property(self, scaler_name, name):
        raise NotImplementedError

    def start_count(self):
        raise NotImplementedError

    def stop_count(self):
        raise NotImplementedError


    def run(self, command):
        raise NotImplementedError

